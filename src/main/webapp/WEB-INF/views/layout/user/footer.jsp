<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<body>

	<!-- Start Footer -->
	<footer class="bg-dark" id="tempaltemo_footer">
		<div class="container">
			<div class="row">

				<div class="col-md-4 pt-5">
					<h2 class="h2 text-success border-bottom pb-3 border-light logo">Người Nghiện Shoes</h2>
					<ul class="list-unstyled text-light footer-link-list">
						<li><i class="fas fa-map-marker-alt fa-fw"></i> 475A Điện Biên Phủ, Quận Bình Thạnh, Thành phố Hồ Chí Minh</li>
						<li><i class="fa fa-phone fa-fw"></i> <a
							class="text-decoration-none" href="tel:035-6047718">0356047718</a>
						</li>
						<li><i class="fa fa-envelope fa-fw"></i> <a
							class="text-decoration-none" href="mailto:nguoinghienshoes@company.com">nguoinghienshoes@company.com</a>
						</li>
					</ul>
				</div>

				<div class="col-md-4 pt-5">
					<h2 class="h2 text-light border-bottom pb-3 border-light">Danh mục sản phẩm</h2>
					<ul class="list-unstyled text-light footer-link-list">
						
						<c:forEach var="item" items="${categorys}">
						<li><a class="text-decoration-none" href="#">${item.name}</a></li>
						</c:forEach>					
						
					</ul>
				</div>

				<div class="col-md-4 pt-5">
					<h2 class="h2 text-light border-bottom pb-3 border-light">Thông tin về Shop
					</h2>
					<ul class="list-unstyled text-light footer-link-list">
						<c:forEach var="item" items="${menus}">
						<li><a class="text-decoration-none" href="/NguoiNghienShop/${item.url}">${item.name}</a></li>
						</c:forEach>				

					</ul>
				</div>

			</div>

			<div class="row text-light mb-4">
				<div class="col-12 mb-3">
					<div class="w-100 my-3 border-top border-light"></div>
				</div>
				<div class="col-auto me-auto">
					<ul class="list-inline text-left footer-icons">
						<li
							class="list-inline-item border border-light rounded-circle text-center">
							<a class="text-light text-decoration-none" target="_blank"
							href="https://www.facebook.com/Sang.IT.Hutech/"><i
								class="fab fa-facebook-f fa-lg fa-fw"></i></a>
						</li>
						<li
							class="list-inline-item border border-light rounded-circle text-center">
							<a class="text-light text-decoration-none" target="_blank"
							href="#"><i
								class="fab fa-instagram fa-lg fa-fw"></i></a>
						</li>

						<li
							class="list-inline-item border border-light rounded-circle text-center">
							<a class="text-light text-decoration-none" target="_blank"
							href="#"><i
								class="fab fa-linkedin fa-lg fa-fw"></i></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>

		<div class="w-100 bg-black py-3">
			<div class="container">
				<div class="row pt-2">
					<div class="col-12">
						<p class="text-left text-light">
							Copyright &copy; 2022 Công ty | TNHH MTV Khoan Bê <a
								rel="sponsored" href="https://www.facebook.com/Sang.IT.Hutech/" target="_blank">Sang Nước Biển</a>
						</p>
					</div>
				</div>
			</div>
		</div>

	</footer>
	<!-- End Footer -->

	
</body>
