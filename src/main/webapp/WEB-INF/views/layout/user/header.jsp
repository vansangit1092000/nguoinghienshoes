<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<body>
	<!-- Start Top Nav -->
	<nav
		class="navbar navbar-expand-lg bg-dark navbar-light d-none d-lg-block"
		id="templatemo_nav_top">
		<div class="container text-light">
			<div class="w-100 d-flex justify-content-between">
				<div>
					<i class="fa fa-envelope mx-2"></i> <a
						class="navbar-sm-brand text-light text-decoration-none"
						href="mailto:nguoinghienshoes@company.com">nguoinghienshoes@company.com</a> <i
						class="fa fa-phone mx-2"></i> <a
						class="navbar-sm-brand text-light text-decoration-none"
						href="tel:035-6047718">035.604.7718</a>
				</div>
				<div>
					<a class="text-light" href="https://www.facebook.com/Sang.IT.Hutech/"
						target="_blank" rel="sponsored"><i
						class="fab fa-facebook-f fa-sm fa-fw me-2"></i></a> <a
						class="text-light" href="https://www.instagram.com/sangseawater/"
						target="_blank"><i class="fab fa-instagram fa-sm fa-fw me-2"></i></a>
				</div>
			</div>
		</div>
	</nav>
	<!-- Close Top Nav -->


	<!-- Header -->
	<nav class="navbar navbar-expand-lg navbar-light shadow">
		<div
			class="container d-flex justify-content-between align-items-center">

			<a class="navbar-brand text-success logo h1 align-self-center"
				href="/NguoiNghienShop"> NN Shoes </a>

			<button class="navbar-toggler border-0" type="button"
				data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div
				class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between"
				id="templatemo_main_nav">
				<div class="flex-fill">
					<ul
						class="nav navbar-nav d-flex justify-content-between mx-lg-auto">
						<c:forEach var="item" items="${menus}">
							<li class="nav-item"><a class="nav-link" href="/NguoiNghienShop/${item.url}"> ${item.name}</a>
						</li>
						</c:forEach>				
						
					</ul>				
					
				</div>
				<div class="navbar align-self-center d-flex">
					<div
						class="d-lg-none flex-sm-fill mt-3 mb-4 col-7 col-sm-auto pr-3">
						<div class="input-group">
							<input type="text" class="form-control" id="inputMobileSearch"
								placeholder="Search ...">
							<div class="input-group-text">
								<i class="fa fa-fw fa-search"></i>
							</div>
						</div>
					</div>
					<a class="nav-icon d-none d-lg-inline" href="#"
						data-bs-toggle="modal" data-bs-target="#templatemo_search"> <i
						class="fa fa-fw fa-search text-dark mr-2"></i>
					</a> <a class="nav-icon position-relative text-decoration-none"
						href="<c:url value="/gio-hang"/>"> <i
						class="fa fa-fw fa-cart-arrow-down text-dark mr-1"></i> <span
						class="position-absolute top-0 left-100 translate-middle badge rounded-pill bg-light text-dark">${TotalQuanty}</span>
					</a> 
					
					
					<div class="dropdown">
					  <a class="dropbtn" href="<c:url value="/dang-nhap"/>" style="padding: 0; width: 20px"><i class="fa fa-fw fa-user text-dark mr-3"> </i>  </a>
							<c:if test="${ empty LoginInfor}">
							 <div class="dropdown-content">
                                <a href="<c:url value="/dang-nhap"/>">Đăng nhập</a>
                                <a href="<c:url value="/dang-ky"/>">Đăng ký</a>
                            </div>   
                            </c:if>

						<c:if test="${ not empty LoginInfor}">
							 <i style="width: 20px">  Xin chào:  ${LoginInfor.fullname} </i>
                            <div class="dropdown-content">
                                <a href="<c:url value="/dang-xuat"/>">Đăng xuất</a>
                                
                            </div>
						</c:if>
                     
                    </div>	
					
				</div>
			</div>

		</div>
		
	</nav>
		<!-- Modal -->
	<div class="modal fade bg-white" id="templatemo_search" tabindex="-1"
		role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document" >
			<div class="w-100 pt-1 mb-5 text-right">
				<button type="button" class="btn-close" data-bs-dismiss="modal"
					aria-label="Close"></button>
			</div>
			<form action="search" method="POST"
				class="modal-content modal-body border-0 p-0">
				<div class="input-group mb-2">
					<input type="text" class="form-control" id="inputModalSearch"
						name="name" placeholder="Search ...">
					<button type="submit"
						class="input-group-text bg-success text-light">
						<i class="fa fa-fw fa-search text-white"></i>
					</button>
				</div>
			</form>
		</div>
	</div>
	<!-- Close Header -->
</body>
