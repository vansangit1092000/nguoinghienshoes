<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head> 
<title><decorator:title default="Master-layout"/></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="apple-touch-icon"
	href="<c:url value="/assets/img/apple-icon.png"/>">
<link rel="shortcut icon" type="image/x-icon"
	href="<c:url value="/assets/img/favicon.ico"/>">

 <link rel="stylesheet" href="<c:url value="/assets/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/assets/css/templatemo.css"/>">
    <link rel="stylesheet" href="<c:url value="/assets/css/custom.css"/>">

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="<c:url value="/assets/css/fontawesome.min.css"/>">
    <!-- Slick -->
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/slick.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/slick-theme.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/cate.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/dropdown.css"/>">
    <decorator:head/>
</head>

<body>
	
	<%@include file="/WEB-INF/views/layout/user/header.jsp"%>

	<decorator:body />
	
	<%@include file="/WEB-INF/views/layout/user/footer.jsp"%>


</body>
<!-- Start Script -->
	<script src="<c:url value="/assets/js/jquery-1.11.0.min.js"/>"></script>
	<script src="<c:url value="/assets/js/jquery-migrate-1.2.1.min.js"/>"></script>
	<script src="<c:url value="/assets/js/bootstrap.bundle.min.js"/>"></script>
	<script src="<c:url value="/assets/js/templatemo.js"/>"></script>
	<script src="<c:url value="/assets/js/custom.js"/>"></script>
	<!-- End Script -->
	
	<!-- Start Slider Script -->
    <script src="<c:url value="/assets/js/slick.min.js"/>"></script>
    <script>
        $('#carousel-related-product').slick({
            infinite: true,
            arrows: false,
            slidesToShow: 4,
            slidesToScroll: 3,
            dots: true,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 3
                    }
                }
            ]
        });
    </script>
    
    <decorator:getProperty property="page.script"></decorator:getProperty>
    <!-- End Slider Script -->
</html>
