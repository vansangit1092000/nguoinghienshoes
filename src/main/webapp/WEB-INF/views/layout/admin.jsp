<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta name="description" content="" />
<meta name="author" content="" />
<title><decorator:title default="Master-layout" /></title>
<link
	href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css"
	rel="stylesheet" />
<link href="<c:url value="/assets/admin/css/styles.css"/>"
	rel="stylesheet" />
<script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js"
	crossorigin="anonymous"></script>
<decorator:head />
</head>

<body class="sb-nav-fixed">
	<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
		<!-- Navbar Brand-->
		<a class="navbar-brand ps-3" href="<c:url value="/admin"/>">NN
			SHOES ADMIN</a>
		<button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0"
			id="sidebarToggle" href="#!">
			<i class="fas fa-bars"></i>
		</button>
		<form
			class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
			<div class="input-group"></div>
		</form>
			
			
		<!-- Navbar-->
				
		<c:if test="${ not empty LoginAdmin}">
							 <i style="width: 20px">  Xin chào:  ${LoginAdmin.fullname} </i>
                            <div class="dropdown-content">
                                <a href="<c:url value="/logout"/>">Đăng xuất</a>
                                
                            </div>
						</c:if>
		<ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" id="navbarDropdown" href="#"
				role="button" data-bs-toggle="dropdown" aria-expanded="false"><i> Xin chào:  ${LoginAdmin.fullname}</i><i class="fas fa-user fa-fw"></i></a>
				<ul class="dropdown-menu dropdown-menu-end"
					aria-labelledby="navbarDropdown">
					<li><a class="dropdown-item" href="<c:url value="/dang-xuat"/>">Đăng xuất</a></li>
				</ul></li>
		</ul>
	</nav>
	<div id="layoutSidenav">
		<div id="layoutSidenav_nav">
			<nav class="sb-sidenav accordion sb-sidenav-dark"
				id="sidenavAccordion">
				<div class="sb-sidenav-menu">
					<div class="nav">
						<a class="nav-link" href="<c:url value="/admin"/>">
							<div class="sb-nav-link-icon">
								<i class="fas fa-tachometer-alt"></i>
							</div> Trang Chủ
						</a>
						<div class="sb-sidenav-menu-heading">QUẢN LÝ CỬA HÀNG</div>

						<a class="nav-link collapsed"
							href="<c:url value="/admin/category"/>">
							<div class="sb-nav-link-icon">
								<i class="fas fa-book-open"></i>
							</div> Quản lý danh mục giày
						</a> <a class="nav-link collapsed"
							href="<c:url value="/admin/product"/>">
							<div class="sb-nav-link-icon">
								<i class="fas fa-book-open"></i>
							</div> Quản lý sản phẩm
						</a> <a class="nav-link collapsed"
							href="<c:url value="/admin/image"/>">
							<div class="sb-nav-link-icon">
								<i class="fas fa-book-open"></i>
							</div> Quản lý Hình
						</a> </a> <a class="nav-link collapsed"
							href="<c:url value="/admin/orders"/>">
							<div class="sb-nav-link-icon">
								<i class="fas fa-book-open"></i>
							</div> Quản lý đơn hàng
						</a> <a class="nav-link collapsed" href="<c:url value="/admin/user"/>">
							<div class="sb-nav-link-icon">
								<i class="fas fa-book-open"></i>
							</div> Quản lý người dùng
						</a> <a class="nav-link collapsed"
							href="<c:url value="/admin/slides"/>">
							<div class="sb-nav-link-icon">
								<i class="fas fa-book-open"></i>
							</div> Quản lý Slides
						</a>





						<div class="sb-sidenav-menu-heading">QUẢN LÝ NHÂN VIÊN</div>
						<a class="nav-link" href="<c:url value="/admin/admin"/>">
							<div class="sb-nav-link-icon">
								<i class="fas fa-table"></i>
							</div> Danh sách nhân viên
						</a>
					</div>
				</div>

			</nav>
		</div>

		<div id="layoutSidenav_content">

			<decorator:body />

		</div>
	</div>




	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		crossorigin="anonymous"></script>
	<script src="<c:url value="/assets/admin/js/scripts.js"/>"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"
		crossorigin="anonymous"></script>
	<script
		src="<c:url value="/assets/admin/assets/demo/chart-area-demo.js"/>"></script>
	<script
		src="<c:url value="/assets/admin/assets/demo/chart-bar-demo.js"/>"></script>
	<script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest"
		crossorigin="anonymous"></script>
	<script
		src="<c:url value="/assets/admin/js/datatables-simple-demo.js"/>"></script>
</body>
</html>