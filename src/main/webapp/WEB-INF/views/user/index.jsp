<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Trang chủ</title>
</head>
<body>
	<!-- Start Banner Hero -->


	<div id="template-mo-zay-hero-carousel" class="carousel slide"
		data-bs-ride="carousel">
		<ol class="carousel-indicators">
			<li data-bs-target="#template-mo-zay-hero-carousel"
				data-bs-slide-to="0" class="active"></li>
			<li data-bs-target="#template-mo-zay-hero-carousel"
				data-bs-slide-to="1"></li>
			<li data-bs-target="#template-mo-zay-hero-carousel"
				data-bs-slide-to="2"></li>
		</ol>
		<div class="carousel-inner">
			<c:forEach var="item" items="${slides}" varStatus="index">
				<c:if test="${index.first}">
					<div class="carousel-item active">
				</c:if>
				<c:if test="${not index.first}">
					<div class="carousel-item">
				</c:if>
				<div class="container">
					<div class="row p-5">
                        <img class="img-fluid" style="width: 2200px; height: 591px " 
								src="<c:url value="/assets/img/${item.img }"/> "/>   
                    </div>			
				</div>
			</div>
			</c:forEach>
		
	</div>
	<a class="carousel-control-prev text-decoration-none w-auto ps-3"
		href="#template-mo-zay-hero-carousel" role="button"
		data-bs-slide="prev"> <i class="fas fa-chevron-left"></i>
	</a>
	<a class="carousel-control-next text-decoration-none w-auto pe-3"
		href="#template-mo-zay-hero-carousel" role="button"
		data-bs-slide="next"> <i class="fas fa-chevron-right"></i>
	</a>
	</div>
	<!-- End Banner Hero -->


	<!-- Start Categories of The Month -->
	<section class="container py-5">
		<div class="row text-center pt-3">
        <div class="col-lg-6 m-auto">
            <h1 style="font-weight: bold">THƯƠNG HIỆU NỔI TIẾNG</h1>
            <p>
                Những đôi giày đến từ các thương hiệu nổi tiếng trên toàn thế giới. </p>
            <p>
                Shop cam kết mang đến cho bạn những đôi giày chất lượng và những trải nghiệm tuyệt vời nhất.

            </p>
        </div>
    </div>
		 <div class="row">
			<c:forEach var="item" items="${category}">
				<div class="col-12 col-md-4 p-5 mt-3">
					<a href="<c:url value="/san-pham/${item.id }"/>"><img class="rounded-circle img-fluid border" style="margin-left: 30px;"
						src="<c:url value="/assets/img/${item.img }"/>"
						class="rounded-circle img-fluid border"></a>
					<h5 class="text-center mt-3 mb-3">${item.name }</h5>
					<p class="text-center">
						<a class="btn btn-success" href="<c:url value="/san-pham/${item.id }"/>">Mua ngay</a>
					</p>
				</div>
			</c:forEach>
		</div> 
	</section>
	<!-- End Categories of The Month -->


	<!--Sản phẩm mới -->
	<section class="bg-light">
		<div class="container py-5">
			<div class="row text-center py-3">
				<div class="col-lg-6 m-auto">
					<h1>Sản phẩm mới</h1>
					<p>Các sản phẩm của shop luôn luôn cập nhật trên website thường xuyên 24/24</p>
				</div>
			</div>
			<div class="row">
				<c:forEach var="item" items="${productpn}">
					<div class="col-12 col-md-4 mb-4">
						<div class="card h-100">
							<a href="<c:url value="/chi-tiet-san-pham/${item.id_product}"/>"> <img
								src="<c:url value="/assets/img/${item.link}"/>"
								class="card-img-top" alt="..." style="height: 414px; width: 414px;">
							</a>
							<div class="card-body">
								<ul class="list-unstyled d-flex justify-content-between">
									<li><i class="text-warning fa fa-star"></i> <i
										class="text-warning fa fa-star"></i> <i
										class="text-warning fa fa-star"></i> <i
										class="text-warning fa fa-star"></i> <i
										class="text-warning fa fa-star"></i></li>
																						
									<li class="text-muted text-right">
									<fmt:formatNumber type="number" groupingUsed="true" value="${item.price} "/> ₫ 
									</li>
								</ul>
								<a href="shop-single.html"
									class="h2 text-decoration-none text-dark">${item.name} - ${item.colors} </a>
								<p class="card-text">${item.detail}</p>
								<p class="text-muted">Tình trạng: Còn hàng</p>
							</div>
						</div>
					</div>
				</c:forEach>

			</div>
		</div>
	</section>
	<!-- End Sản phẩm mới-->

	<!--Sản phẩm nổi bật -->
	<section class="bg-white">
		<div class="container py-5">
			<div class="row text-center py-3">
				<div class="col-lg-6 m-auto">
					<h1>Sản phẩm nổi bật</h1>
					<p>Các sản phẩm được các bạn trẻ ưa thích và được săn đón nhiều nhất</p>
				</div>
			</div>
			<div class="row">
				<c:forEach var="item" items="${productpf}">
					<div class="col-12 col-md-4 mb-4">
						<div class="card h-100">
							<a href="<c:url value="/chi-tiet-san-pham/${item.id_product}"/>"> <img
								src="<c:url value="/assets/img/${item.link}"/>" style="height: 414px; width: 414px;"
								class="card-img-top" alt="...">
							</a>
							<div class="card-body">
								<ul class="list-unstyled d-flex justify-content-between">
									<li><i class="text-warning fa fa-star"></i> <i
										class="text-warning fa fa-star"></i> <i
										class="text-warning fa fa-star"></i> <i
										class="text-warning fa fa-star"></i> <i
										class="text-warning fa fa-star"></i></li>
																	
									
									<li class="text-muted text-right"><fmt:formatNumber type="number" groupingUsed="true" value="${item.price} "/> ₫ </li>
								</ul>
								<a href="shop-single.html"
									class="h2 text-decoration-none text-dark">${item.name} - ${item.colors} </a>
								<p class="card-text">${item.detail}</p>
								<p class="text-muted">Tình trạng: Còn hàng</p>
							</div>
						</div>
					</div>
				</c:forEach>
				</div>
			</div>
		</div>
	</section>
	<!-- End Sản phẩm nổi bật-->
</body>
</html>

