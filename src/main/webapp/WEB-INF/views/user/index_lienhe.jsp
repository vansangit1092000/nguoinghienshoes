<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>

<head>
<meta charset="UTF-8">
<title>Liên hệ</title>
</head>

<link rel="stylesheet"
	href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
<link rel="stylesheet"
	href="<c:url value="assets/css/fontawesome.min.css"/>">


<!-- Load map styles -->
<link rel="stylesheet"
	href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
	integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
	crossorigin="" />


<div class="container-fluid bg-light py-5">
	<div class="col-md-6 m-auto text-center">
		<h1 style="color: red; font-size: 60px">LIÊN HỆ SHOP</h1>
		<p>Người Nghiện Shoes - Mang đến những đôi giày chất lượng và giá
			tốt nhất.</p>
		<p>Hãy cho chúng mình biết cảm nhận của bạn về Shop. Chúng mình sẽ
			cố gắng hoàn thiện để phục vụ tốt nhất ^ ^</p>
		<h3>${status}</h3>
	</div>	
</div>


<!-- Start Contact -->
<div class="container py-5">
	<div class="row py-5">

		<form:form class="col-md-9 m-auto" action="lien-he" method="post"
			modelAttribute="feedbacks">
			<div class="row">
				<div class="form-group col-md-6 mb-3">
					<label for="inputname">Họ và tên</label>
					<form:input type="text" class="form-control mt-1" id="name"
						placeholder="Họ và tên" path="name" />
				</div>
				<div class="form-group col-md-6 mb-3">
					<label for="inputemail">Email</label>
					<form:input type="text" class="form-control mt-1" id="email"
						path="email" />
				</div>
			</div>
			<div class="mb-3">
				<label for="inputsubject">Điện thoại</label>
				<form:input class="form-control mt-1" id="phone" path="phone" />
			</div>
			<div class="mb-3">
				<label for="inputsubject">Địa chỉ</label> 
				<form:input class="form-control mt-1" id="address" path="address" />
			</div>
			<div class="mb-3">
				<label for="inputmessage">Lời nhắn</label>
				<form:textarea class="form-control mt-1" id="message" name="message"
					placeholder="Message" rows="8" path="message"/>
			</div>
			<div class="row">
				<div class="col text-end mt-2">
					<button type="submit" class="btn btn-success btn-lg px-3">Liên
						hệ ngay</button>
				</div>
			</div>
		</form:form>
	</div>
</div>


<!-- Start Map -->
<div id="mapid" style="width: 100%; height: 500px;"></div>
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
	integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
	crossorigin=""></script>
<script>
	var mymap = L.map('mapid').setView([ 10.835226, 106.773131, 13 ], 15);

	L
			.tileLayer(
					'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
					{
						maxZoom : 20,
						attribution : 'NN Shoes | Template Design by <a href="https://templatemo.com/">Templatemo</a> | Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, '
								+ '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, '
								+ 'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
						id : 'mapbox/streets-v11',
						tileSize : 512,
						zoomOffset : -1
					}).addTo(mymap);

	L
			.marker([ 10.835226, 106.773131, 13 ])
			.addTo(mymap)
			.bindPopup(
					"<b>Người Nghiện Shoes</b> <br />Mang đến như đôi giày chất lượng nhất<br />Hotline: 035.604.7718")
			.openPopup();

	mymap.scrollWheelZoom.disable();
	mymap.touchZoom.disable();
</script>
