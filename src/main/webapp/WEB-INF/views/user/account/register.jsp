<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>

	<div class="main-w3layouts wrapper">
		<h1>Đăng ký tài khoản</h1>
		<div class="main-agileinfo">
			<div class="agileits-top">
				<form:form action="dang-ky" method="POST" modelAttribute="user">
					<form:input type="text"  placeholder="Tên đăng nhập" path="username" required="Điền tên đăng nhập" />
					<br>
					<form:input type="password" placeholder="Mật khẩu" path="password" required="Điền mật khẩu"/>
					<br>
					<form:input type="text" placeholder="Họ và tên" path="fullname" required="Điền tên"/>
					<br>
					<form:input type="email"  placeholder="Email" path="email" required="Điền email"/>
					<br>
					<form:input type="text"  placeholder="Số điện thoại" path="phone" required="Điền số điện thoại"/>
					<br>
					<form:input type="text"  placeholder="Địa chỉ" path="address" required="Điền địa chỉ"/>						
					<input type="submit" value="ĐĂNG KÝ">
				</form:form>
				<p>Bạn đã có tài khoản? <a href="<c:url value="/dang-nhap"/>"> Đăng nhập</a></p>
			</div>
		</div>
			<ul class="colorlib-bubbles">
				<li></li>
				<li></li>
				<li></li>
				<li></li>
				<li></li>
				<li></li>
				<li></li>
				<li></li>
				<li></li>
				<li></li>
			</ul>
	</div>
