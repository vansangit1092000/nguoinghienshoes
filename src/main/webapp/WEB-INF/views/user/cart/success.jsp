<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>

<head>
<meta charset="UTF-8">
<title>Đặt hàng thành công</title>
</head>

<style>
.imggh {
	margin: 20px 0 0 50px;
	width: 1100px;
	height: 650px;
}
</style>

<section class="bg-light">
	<div class="container pb-5">
		<div class="container pb-5">

			<img class="imggh" src="<c:url value="/assets/img/thanhcong.png"/> " />
			<div class="row" style="margin-top: 20px">
				<div class="col text-end mt-2">
					<button class="btn btn-outline-success btn-lg px-3" onclick="myFunction()"
						style="margin: 0 550px 0 0;">Trở về Trang Chủ</button>
				</div>
			</div>

		</div>
	</div>
</section>

<script>
function myFunction() {
	 window.location = "trang-chu";
	}
</script>
