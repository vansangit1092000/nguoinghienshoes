<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>


<head>
<meta charset="UTF-8">
<title>Đặt hàng</title>
</head>

<body>
	<div class="shopping-cart">
		<div class="px-4 px-lg-0">

			<div class="pb-5">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 p-5 bg-white rounded shadow-sm mb-5">

							<!-- Shopping cart table -->
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th scope="col" class="border-0 bg-light">
												<div class="p-2 px-3 text-uppercase">Sản Phẩm</div>
											</th>

											<th scope="col" class="border-0 bg-light">
												<div class="py-2 text-uppercase">Size Giày</div>
											</th>

											<th scope="col" class="border-0 bg-light">
												<div class="py-2 text-uppercase">Số Lượng</div>
											</th>
											<th scope="col" class="border-0 bg-light">
												<div class="py-2 text-uppercase">Đơn Giá</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="item" items="${Cart}">
										<tr>
											<th scope="row">
												<div class="p-2">
													<img src="<c:url value="/assets/img/${item.value.product.link}"/>" width="70"
														class="img-fluid rounded shadow-sm">
													<div class="ml-3 d-inline-block align-middle">
														<h5 class="mb-0">
															<p class="text-dark d-inline-block">${item.value.product.name}</p>
														</h5>
														<span class="text-muted font-weight-normal font-italic">${item.value.product.colors}</span>
													</div>
												</div>
											</th>



											<td class="align-middle"><strong
												style="text-align: center; padding-left: 27px">38</strong></td>
											<td class="align-middle "><strong
												style="text-align: center; padding-left: 35px">${item.value.quanty}</strong></td>
											<td class="align-middle"><strong>${item.value.product.price}</strong></td>
										</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- End -->
						</div>
					</div>

					<div class="row py-5 p-4 bg-white rounded shadow-sm">
						<div class="col-lg-6">
						
							<form:form class="col-md-9 m-auto" action="thanh-toan"
								method="post" modelAttribute="orders">

								<div class="row">

									<div class="form-group col-md-6 mb-3">
										<label for="inputname">Họ và tên nguời nhận</label> 
										<form:input type="text" class="form-control mt-1" id="name" path="ship_name" required="Vui lòng điền họ tên" />									
									</div>
									
									<div class="form-group col-md-6 mb-3">
										<label for="inputemail">Email</label> 
										<form:input type="text" class="form-control mt-1" id="email" path="ship_email" required="Vui lòng điền email" />	
									</div>

									<div class="mb-3">
										<label for="inputsubject">Điện thoại</label> 
										<form:input class="form-control mt-1" id="phone" path="ship_mobile" required="Vui lòng điền số điện thoại"/>	
									</div>
									
									<div class="mb-3">
										<label for="inputsubject">Địa chỉ</label>
										<form:input type="text" class="form-control mt-1" id="address" path="ship_address" required="Vui lòng điền địa chỉ"/>	

									</div>
									<div class="row">
										<div class="col text-end mt-2">
											<button type="submit" class="btn btn-success btn-lg px-3">Gửi
												đơn hàng</button>
										</div>
									</div>
								</div>
							</form:form>
						</div>
						<div class="col-lg-6">
							<div
								class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Thành
								tiền</div>
							<div class="p-4">
								<ul class="list-unstyled mb-4">
									<li class="d-flex justify-content-between py-3 border-bottom"><strong
										class="text-muted">Tổng số lượng:</strong><strong>${TotalQuanty}</strong></li>
									<li class="d-flex justify-content-between py-3 border-bottom"><strong
										class="text-muted">Phí ship:</strong><strong>Free
											ship</strong></li>

									<li class="d-flex justify-content-between py-3 border-bottom">
										<strong class="text-muted">Tổng thanh toán</strong>
										<h5 class="font-weight-bold">
											<fmt:formatNumber type="number" groupingUsed="true"
												value="${TotalPrice} " />
											₫
										</h5>
									</li>
								</ul>
								<button id="button-addon3" type="button"
									class="btn btn-dark rounded-pill py-2 btn-block">Trở
									về giỏ hàng</button>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


</body>
