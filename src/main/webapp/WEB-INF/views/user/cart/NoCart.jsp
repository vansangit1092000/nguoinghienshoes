<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>


<head>
<meta charset="UTF-8">
<title>Giỏ Hàng Rỗng</title>
</head>

<body>

	<section class="bg-light">
		<div class="container pb-5">
			<div class="container pb-5">

				<img src="<c:url value="/assets/img/empty-cart.png"/>"
					style="margin: 100px 100px 100px 400px; text-align: center">

				<div class="row">
					<div class="col text-end mt-2">
						<button type="button" class="btn btn-outline-success btn-lg px-3" style="margin-right: 540px"
							onclick="myFunction()">Tiếp tục mua hàng</button>
					</div>
				</div>

			</div>
		</div>
	</section>

	<script>
		
		function myFunction() {
			window.location = "san-pham";
		}
	</script>
</body>
