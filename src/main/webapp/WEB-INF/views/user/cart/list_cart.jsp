<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<head>
<meta charset="UTF-8">
<title>Giỏ Hàng</title>
</head>
<body>

	<style>
.oke {
	text-decoration: none;
	color: white;
	font-weight: bolder;
}

.oke:hover {
	color: greenyellow
}
</style>

	<div class="shopping-cart">
		<div class="px-4 px-lg-0">
			<div class="pb-5">
				<div class="container">
					<div class="row">

						<div class="col-lg-12 p-5 bg-white rounded shadow-sm mb-5">

							<!-- Shopping cart table -->
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th scope="col" class="border-0 bg-light">
												<div class="p-2 px-3 text-uppercase">Sản Phẩm</div>
											</th>

											<th scope="col" class="border-0 bg-light">
												<div class="py-2 text-uppercase">Size Giày</div>
											</th>

											<th scope="col" class="border-0 bg-light">
												<div class="py-2 text-uppercase">Số Lượng</div>
											</th>
											<th scope="col" class="border-0 bg-light">
												<div class="py-2 text-uppercase">Đơn Giá</div>
											</th>
											<th scope="col" class="border-0 bg-light">
												<div class="py-2 text-uppercase">Thành tiền</div>
											</th>
											<th scope="col" class="border-0 bg-light">
												<div class="py-2 text-uppercase">Chỉnh sửa</div>
											</th>
										</tr>
									</thead>

									<tbody>

										<c:forEach var="item" items="${Cart}">

											<tr>
												<th scope="row">
													<div class="p-2">
														<img
															src="<c:url value="/assets/img/${item.value.product.link}"/>"
															width="70" class="img-fluid rounded shadow-sm">
														<div class="ml-3 d-inline-block align-middle">
															<h5 class="mb-0">
																<p class="text-dark d-inline-block">${item.value.product.name}</p>
															</h5>
															<span class="text-muted font-weight-normal font-italic">${item.value.product.colors}</span>
														</div>
													</div>
												</th>

												<td class="align-middle"><input type="number"
													name="quant[2]" class="form-control input-number"
													value="38" min="38" max="44"
													style="width: 70px; text-align: center"></td>
												<td class="align-middle"><input type="number"
													name="quant[1]" class="form-control input-number" id="quanty-cart-${item.key}"
													value="${item.value.quanty}" min="1" max="10"
													style="width: 70px; text-align: center"
													></td>
												<td class="align-middle"><strong> <fmt:formatNumber
															type="number" groupingUsed="true"
															value="${item.value.product.price} " /> ₫
												</strong></td>

												<td class="align-middle"><strong>
												<fmt:formatNumber
															type="number" groupingUsed="true"
															value="${item.value.totalPrice}" /> ₫
												</strong></td>

												<td class="align-middle">
													<p class="text-dark">
														<button type="button" data-id="${item.key}"
															class="btn btn-danger oke edit-cart">Cập nhật</button>
														<a href="<c:url value="/DeleteCart/${item.key}"/>"
															type="button" class="btn btn-danger oke">Xóa</a>
													</p>
												</td>
											</tr>
										</c:forEach>
									</tbody>

								</table>
							</div>
							<!-- End -->
						</div>
					</div>
					<div class="row py-5 p-4 bg-white rounded shadow-sm">
						<div class="col-lg-6">
							<div
								class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Voucher</div>
							<div class="p-4">
								<div class="input-group mb-4 border rounded-pill p-2">
									<input type="text" name="voucher" placeholder="Nhập Voucher"
										aria-describedby="button-addon3" class="form-control border-0">

									<div class="input-group-append border-0">
										<button id="button-addon3" type="submit"
											class="btn btn-dark px-4 rounded-pill">
											<i class="fa fa-gift mr-2"></i>Sử dụng
										</button>
									</div>
								</div>

								<a id="button-addon3" type="button"
									class="btn btn-success px-4 rounded-pill"
									style="margin-top: 130px" href="<c:url value="/san-pham"/>">
									Tiếp tục mua hàng </a>
							</div>

						</div>

						<div class="col-lg-6">
							<div
								class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Thành
								tiền</div>
							<div class="p-4">
								<ul class="list-unstyled mb-4">
									<li class="d-flex justify-content-between py-3 border-bottom"><strong
										class="text-muted">Tổng số lượng:</strong><strong>${TotalQuanty}</strong></li>
									<li class="d-flex justify-content-between py-3 border-bottom"><strong
										class="text-muted">Phí ship:</strong><strong>Free
											ship</strong></li>

									<li class="d-flex justify-content-between py-3 border-bottom">
										<strong class="text-muted">Tổng thanh toán</strong>
										<h5 class="font-weight-bold">
											<fmt:formatNumber type="number" groupingUsed="true"
												value="${TotalPrice} " />
											₫
										</h5>
									</li>
								</ul>
								<button id="button-addon3 " type="button"
									class="btn btn-dark px-4 rounded-pill delete-all">Xóa tất cả</button>
								<button id="button-addon3" type="button"
									class="btn btn-dark px-4 rounded-pill checkout">Đặt hàng</button>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div id="message"></div>
	<content tag="script"> <script
		src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script> <script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script> <script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script> <script
		src="<c:url value="/assets/js//them.js"/>"></script>
        
        <script>
        $(document).ready(function(){
            $(".edit-cart").click(function(){
            	var id = $(this).data("id");
  				var quanty = $("#quanty-cart-"+id).val();	
  				window.location = "EditCart/"+id+"/"+quanty;             
            });    
            $(".delete-all").click(function(){	
  				window.location = "gio-hang-rong";   
            }); 
        
            $(".checkout").click(function(){	
  				window.location = "thanh-toan";   
            });
            
        });
        </script>
		
		</content>
	>

</body>
