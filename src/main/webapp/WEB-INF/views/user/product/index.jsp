<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sản phẩm</title>
</head>
<body>
	
	<!-- Start Content -->
	<div class="container py-5">
		<div class="row">
	
			<div class="col-lg-3">
				<h1>Danh mục sản phẩm</h1>
				<ul id="accordion" class="accordion">
					<c:forEach var="item" items="${categorys}">
					<li>
						<div class="link">
							<i class="fa fa-shoe-prints"></i> 
							<a href="<c:url value="/san-pham/${item.id }"/>">${item.name }</a> <i
								class="fa fa-chevron-down"></i>
						</div>
						
					</li> 
					</c:forEach>
				</ul>
			</div>

			<div class="col-lg-9">
				<div class="row">
					<div class="col-md-6">
						<ul class="list-inline shop-top-menu pb-3 pt-1">
							<li class="list-inline-item"><a
								class="h3 text-dark text-decoration-none mr-3"  href="/NguoiNghienShop/san-pham">Tất cả</a>
							</li>
							<li class="list-inline-item"><a
								class="h3 text-dark text-decoration-none mr-3" href="#">Giá giảm dần</a>
							</li>
							<li class="list-inline-item"><a
								class="h3 text-dark text-decoration-none" href="#">Giá tăng dần</a></li>
						</ul>
					</div>
					
				</div>

				<div class="row">					
					<c:forEach var="item" items="${productsPaginates}">
						<div class="col-md-4">
							<div class="card mb-4 product-wap rounded-0">
								<div class="card rounded-0">
									<img class="card-img rounded-0 img-fluid"
										src="<c:url value="/assets/img/${item.link}"/>" style="height: 302px; width: 302px;">
									<div
										class="card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center">
										<ul class="list-unstyled">
											<li><a class="btn btn-success text-white"
												href="shop-single.html"><i class="far fa-heart"></i></a></li>
											<li><a class="btn btn-success text-white mt-2"
												href="<c:url value="/chi-tiet-san-pham/${item.id_product}"/>"><i
													class="far fa-eye"></i></a></li>
											<li><a class="btn btn-success text-white mt-2"
												href="<c:url value="/AddCart/${item.id_product}"/>"><i
													class="fas fa-cart-plus"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-body">
									<a class="h3 text-decoration-none" style="margin-left: 45px">${item.name}
										- ${item.colors}</a>
									<ul
										class="w-100 list-unstyled d-flex justify-content-between mb-0">

									</ul>
									<ul class="list-unstyled d-flex justify-content-center mb-1">
										<li><i class="text-warning fa fa-star"></i> <i
											class="text-warning fa fa-star"></i> <i
											class="text-warning fa fa-star"></i> <i
											class="text-muted fa fa-star"></i> <i
											class="text-muted fa fa-star"></i></li>
									</ul>
									<p class="text-center mb-0">
										<fmt:formatNumber type="number" groupingUsed="true"
											value="${item.price} " />
										₫
									</p>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>

				<div div="row">
					<ul class="pagination pagination-lg justify-content-end">
						<li class="page-item"><a class="page-link"
							aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
						</a></li>


						<c:forEach var="item" begin="1" end="${paginateInfo.totalPage }"
							varStatus="loop">
							<c:if test="${ (loop.index) ==  paginateInfo.currentPage }">
								<li class="page-item"><a
									class="page-link active rounded-0 mr-3 shadow-sm border-top-0 border-left-0"
									href="<c:url value="/san-pham-all/${ loop.index }"/>">
										${ loop.index } </a></li>
							</c:if>

							<c:if test="${ (loop.index) !=  paginateInfo.currentPage }">
								<li class="page-item"><a
									class="page-link active rounded-0 mr-3 shadow-sm border-top-0 border-left-0"
									href="<c:url value="/san-pham-all/${ loop.index }"/>">
										${ loop.index } </a></li>
							</c:if>
						</c:forEach>

						<li class="page-item"><a class="page-link" aria-label="Next">
								<span aria-hidden="true">&raquo;</span> <span class="sr-only">Next</span>
						</a></li>
					</ul>


				</div>
			</div>

		</div>
	</div>
	<!-- End Content -->

</body>
</html>