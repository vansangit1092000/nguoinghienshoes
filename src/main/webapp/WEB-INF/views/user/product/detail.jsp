<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>


<head>
<meta charset="UTF-8">
<title>Chi tiết sản phẩm</title>
</head>

	<!-- Open Content -->
	<section class="bg-light">
		<div class="container pb-5">
			<div class="row">
				<div class="col-lg-5 mt-5">
					<div class="card mb-3">
						<img class="card-img img-fluid"
							src="<c:url value="/assets/img/${product.link}"/>"
							alt="Card image cap" id="product-detail">
					</div>
					<div class="row">
						<!--Start Controls-->
						<div class="col-1 align-self-center">
							<a href="#multi-item-example" role="button" data-bs-slide="prev">
								<i class="text-dark fas fa-chevron-left"></i> <span
								class="sr-only">Previous</span>
							</a>
						</div>
						<!--End Controls-->
						<!--Start Carousel Wrapper-->
						<div id="multi-item-example"
							class="col-10 carousel slide carousel-multi-item"
							data-bs-ride="carousel">
							<!--Start Slides-->
							<div class="carousel-inner product-links-wap" role="listbox">

								<!--First slide-->
								<div class="carousel-item active">
									<div class="row">
										<c:forEach var='item' begin='1' end='3'>
											<div class="col-4">
												<a href="#"> <img class="card-img img-fluid"
													src="<c:url value="/assets/img/${img[item].link}"/>">
												</a>
											</div>
										</c:forEach>

									</div>
								</div>
								<!--/.First slide-->

								<!--Second slide-->
								<div class="carousel-item">
									<div class="row">

										<c:forEach var='ite' begin='0' end='2'>

											<div class="col-4">
												<a href="#"> <img class="card-img img-fluid"
													src="<c:url value="/assets/img/${img[ite].link}"/>">
												</a>
											</div>
										</c:forEach>

									</div>
								</div>
							</div>
							<!--End Slides-->
						</div>
						<!--End Carousel Wrapper-->
						<!--Start Controls-->
						<div class="col-1 align-self-center">
							<a href="#multi-item-example" role="button" data-bs-slide="next">
								<i class="text-dark fas fa-chevron-right"></i> <span
								class="sr-only">Next</span>
							</a>
						</div>
						<!--End Controls-->
					</div>
				</div>
				<!-- col end -->
				<div class="col-lg-7 mt-5">
					<div class="card">
						<div class="card-body">
							<h2 style="color: red">${product.name}</h2>
							<p class="h3 py-2">
								<fmt:formatNumber type="number" groupingUsed="true"
									value="${product.price} " />
								₫
							</p>
							<p class="py-2">
								<i class="fa fa-star text-warning"></i> <i
									class="fa fa-star text-warning"></i> <i
									class="fa fa-star text-warning"></i> <i
									class="fa fa-star text-warning"></i> <i
									class="fa fa-star text-warning"></i> <span
									class="list-inline-item text-dark">Đánh giá 5.0 | 36
									Lượt mua</span>
							</p>
							<ul class="list-inline">
								<li class="list-inline-item">
									<h5>Thương hiệu : </h5>
								</li>
								<li class="list-inline-item">
									<p>
										<strong style="font-weight: 900; color: red">${cate.name}</strong>
									</p>
								</li>
							</ul>

							<ul class="list-inline">
								<li class="list-inline-item">
									<h5>Màu sắc : </h5>
								</li>
								<li class="list-inline-item">
									<p>
										<strong style="font-weight: 900;">${product.colors}</strong>
									</p>
								</li>
							</ul>
							<h5>Mô tả sản phẩm:</h5>
							<p>${product.detail}</p>

								<div class="row">
									<div class="col-auto">
										<ul class="list-inline pb-3">
											<li class="list-inline-item"><h5>Size:</h5> <input
												type="hidden" name="product-size" id="product-size"
												value="S"></li>
											<c:forEach var="item" items="${sizes}">
												<li class="list-inline-item"><span
													class="btn btn-success btn-size">${item.name_size}</span></li>
											</c:forEach>
										</ul>
									</div>
								</div>

								<div class="row">
									<div class="col-auto">
										<ul class="list-inline pb-3">
											<li class="list-inline-item text-right">
												<h5>Số lượng:</h5> <input type="hidden"
												name="product-quanity" id="product-quanity" value="1">
											</li>
											<li class="list-inline-item"><span
												class="btn btn-success" id="btn-minus">-</span></li>
											<li class="list-inline-item"><span
												class="badge bg-secondary" id="var-value"
												style="font-size: 18px;">1</span></li>
											<li class="list-inline-item"><span
												class="btn btn-success" id="btn-plus">+</span></li>
										</ul>
									</div>
								</div>
								<div class="row pb-3">
									<div class="col d-grid">
										<button class="btn btn-success btn-lg" onclick="myFunction(${product.id_product}); myFunction1()">Mua ngay</button>
									</div>
									<div class="col d-grid">
										<button class="btn btn-success btn-lg addtocard" onclick="myFunction(${product.id_product})">Thêm vào giỏ hàng</button>
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Close Content -->

	<!-- Start Article -->
	<section class="py-5">
		<div class="container">
			<div class="row text-left p-2 pb-3">
				<h2 style="text-align: center">Sản phẩm liên quan</h2>

			</div>

			<!--Start Carousel Wrapper-->
			<div id="carousel-related-product">
				<c:set var="countList" value="${productcate.size()}" />
				<c:if test="${productcate.size() > 12 }">
					<c:set var="countList" value="12" />
				</c:if>
				<c:forEach var="item" items="${productcate}" begin="1"
					end="${countList}">
					<div class="p-2 pb-3">
						<div class="product-wap card rounded-0">
							<div class="card rounded-0">
								<img class="card-img rounded-0 img-fluid"
									src="<c:url value="/assets/img/${item.link}"/>">
								<div
									class="card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center">
									<ul class="list-unstyled">
										<li><a class="btn btn-success text-white"
											href="shop-single.html"><i class="far fa-heart"></i></a></li>
										<li><a class="btn btn-success text-white mt-2"
											href="<c:url value="/chi-tiet-san-pham/${item.id_product}"/>"><i
												class="far fa-eye"></i></a></li>
										<li><a class="btn btn-success text-white mt-2"
											href="shop-single.html"><i class="fas fa-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body">
								<a class="h3 text-decoration-none" style="margin-left: 45px">${item.name}
									- ${item.colors}</a>

								<ul class="list-unstyled d-flex justify-content-center mb-1">
									<li><i class="text-warning fa fa-star"></i> <i
										class="text-warning fa fa-star"></i> <i
										class="text-warning fa fa-star"></i> <i
										class="text-warning fa fa-star"></i> <i
										class="text-muted fa fa-star"></i></li>
								</ul>
								<p class="text-center mb-0">
									<fmt:formatNumber type="number" groupingUsed="true"
										value="${item.price}" />
									₫
								</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>


		</div>
	</section>
	<!-- End Article -->
	
	<script type="text/javascript">
		function myFunction(id) {
		  window.location = "/NguoiNghienShop/AddCart/"+ id;
		}
		
		function myFunction1() {
			  window.location = "/NguoiNghienShop/gio-hang";
			}
	</script>

