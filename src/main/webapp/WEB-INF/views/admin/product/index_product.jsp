<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<title>Quản lý sản phẩm</title>

<style>
.image {
	height: 100px;
	width: 100px;
}
</style>

<main>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Sản phẩm</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">Trang
					chủ</a></li>
			<li class="breadcrumb-item active">Sản phẩm</li>
		</ol>

		<div class="card mb-4">
			<div class="card-body">
			<button onclick="document.location.href ='/NguoiNghienShop/admin/newProduct';" class="w3-button w3-black">
							Thêm mới
			</button>
				<table id="datatablesSimple">
					<thead>
						<tr style="font-size: larger;">
							<th>STT</th>
							<th>Tên sản phẩm</th>
							<th>Màu</th>
							<th>Thương hiệu</th>
							<th>Sale</th>
							<th>Tiêu đề</th>
							<th>Nổi bật</th>
							<th>Mới</th>
							<th>Mô tả</th>
							<th>Giá tiền</th>
							<th>Size - Số lượng</th>
							<th>Chỉnh sửa</th>
						</tr>
					</thead>
					<tfoot>
						<tr style="font-size: larger;">
							<th>STT</th>
							<th>Tên sản phẩm</th>
							<th>Màu</th>
							<th>Thương hiệu</th>
							<th>Sale</th>
							<th>Tiêu đề</th>
							<th>Nổi bật</th>
							<th>Mới</th>
							<th>Mô tả</th>
							<th>Giá tiền</th>
							<th>Size - Số lượng</th>
							<th>Chỉnh sửa</th>
						</tr>
					</tfoot>
					<tbody>
						<c:set var="i" value="1" />
						<c:forEach var="item" items="${products}">
							<tr style="font-size: 15px;">
								<td><c:out value="${i}" /> <c:set var="i" value="${i+1}" />
								</td>
								<td>${item.name}</td>
								<td>${item.colors}</td>
								<c:forEach var="item_cate" items="${categorys}">
									<c:if test="${item_cate.id == item.id_category }">
										<td>${item_cate.name}</td>
									</c:if>
								</c:forEach>
								
								<c:set  var = "sale" value = " "/>
								<c:if test="${item.sale == 1}">
									<c:set  var = "sale" value = "Có"/>
								</c:if>
								<c:if test="${item.sale == 0}">
									<c:set  var = "sale" value = "Không"/>
								</c:if>
								<td>${sale}</td>
								
								<td>${item.title}</td>
								
								<c:set  var = "highlight" value = " "/>
								<c:if test="${item.highlight == true}">
									<c:set  var = "highlight" value = "Có"/>
								</c:if>
								<c:if test="${item.highlight == false}">
									<c:set  var = "highlight" value = "Không"/>
								</c:if>
								<td>${highlight}</td>
								
								<c:set  var = "new_product" value = " "/>
								<c:if test="${item.new_product == true}">
									<c:set  var = "new_product" value = "Có"/>
								</c:if>
								<c:if test="${item.new_product == false}">
									<c:set  var = "new_product" value = "Không"/>
								</c:if>
								<td>${new_product}</td>
								
								<td>${item.detail}</td>				
								<td><fmt:formatNumber type="number" groupingUsed="true"
										value="${item.price} " /> ₫</td>
								<td>
								<c:forEach var="item_size" items="${sizes}">
									<c:if test="${item_size.id_product == item.id_product }">
										${item_size.name_size} - ${item_size.quantity}
										<br>
									</c:if>

								</c:forEach>
								</td>
														
								<td>
									<button
										onclick="document.location.href ='/NguoiNghienShop/admin/editProduct?id=${item.id_product}';"
										class="w3-button w3-black">Chỉnh sửa</button>
									<button
										onclick="document.location.href ='/NguoiNghienShop/admin/deleteProduct?id=${item.id_product}';"
										class="w3-button w3-black">Xóa</button>
								</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>

