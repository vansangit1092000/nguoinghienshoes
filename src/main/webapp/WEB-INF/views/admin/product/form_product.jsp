<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<head>
<title>Sản phẩm</title>
</head>

<body>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Sản phẩm</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">
					Trang chủ</a></li>
			<li class="breadcrumb-item active">Sản phẩm</li>
		</ol>
		<div align="center">
			<h1>${title}</h1>
			<form:form action="/NguoiNghienShop/admin/newProduct" method="post"
				modelAttribute="product">
				<table>
					<form:hidden path="id_product" />
					<tr>
						<td>Tên:</td>
						<td><form:input path="name" required="Điền tên" /></td>
					</tr>
					<tr>
						<td>Màu:</td>
						<td><form:input path="colors" required="Điền màu" /></td>
					</tr>
					<tr>
						<td>Thương hiệu:</td>
						<td><form:select path="id_category" required="Chọn">
								<c:forEach var="item_cate" items="${categorys}">
									<form:option value="${ item_cate.id}">${item_cate.name}</form:option>
								</c:forEach>
							</form:select></td>
					</tr>
					<tr>

						<td>Tiêu đề:</td>
						<td><form:input path="title" required="Điền tiêu đề" /></td>
					</tr>
					<tr>
						<td>Mô tả:</td>
						<td><form:input path="detail" required="Điền mô tả"/></td>
					</tr>
					<tr>
						<td>Sale:</td>
						<td><form:select path="sale" required="Chọn trạng thái">
								<form:option value="1">Có</form:option>
								<form:option value="0">Không</form:option>
							</form:select></td>
					</tr>

					<tr>
						<td>Nổi bật:</td>
						<td><form:select path="highlight" required="Chọn trạng thái">
								<form:option value="true">Có</form:option>
								<form:option value="false">Không</form:option>
							</form:select></td>
					</tr>

					<tr>
						<td>Mới:</td>
						<td><form:select path="new_product"
								required="Chọn trạng thái">
								<form:option value="true">Có</form:option>
								<form:option value="false">Không</form:option>
							</form:select></td>
					</tr>

					<tr>
						<td>Giá tiền:</td>
						<td><form:input path="price" /></td>
					</tr>
					<tr>
						<td colspan="2" align="center"><input type="submit"
							value="Save"></td>
						<td><button
								onclick="document.location.href ='/NguoiNghienShop/admin/category';"
								class="w3-button w3-black">Quay lại</button></td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
</body>
