<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<title>Quản lý Slides</title>
<style>
.image {
	height: 350px;
	width: 700px;
}
</style>


<main>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Slides</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">Trang
					chủ</a></li>
			<li class="breadcrumb-item active">Slides</li>
		</ol>

		<div class="card mb-4">

			<div class="card-body">
				<button
					onclick="document.location.href ='/NguoiNghienShop/admin/newSlides';"
					class="w3-button w3-black">Thêm mới</button>
				<br> <br>
				<table id="datatablesSimple">
					<thead>
						<tr style="font-size: larger">
							<th>STT</th>
							<th>Tiêu đề</th>
							<th>Hình ảnh</th>
							<th>Chỉnh sửa</th>
						</tr>
					</thead>
					<tfoot>
						<tr style="font-size: larger">
							<th>STT</th>
							<th>Tiêu đề</th>
							<th>Hình ảnh</th>
							<th>Chỉnh sửa</th>
						</tr>
					</tfoot>
					<tbody>
						<c:forEach var="item" items="${slides}" varStatus="status">
							<tr style="font-size: larger;">
								<td>${status.index + 1}</td>
								<td>${item.caption}</td>
								<td style="text-align: center;"><img class="image"
									src="<c:url value="/assets/img/${item.img}"/>" /></td>
								<td>
									<button
										onclick="document.location.href ='/NguoiNghienShop/admin/editSlides?id=${item.id}';"
										class="w3-button w3-black">Chỉnh sửa</button>
									<button
										onclick="document.location.href ='/NguoiNghienShop/admin/deleteSlides?id=${item.id}';"
										class="w3-button w3-black">Xóa</button>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>

