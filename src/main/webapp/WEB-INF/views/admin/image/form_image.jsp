<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<head>
<title>Chỉnh sửa</title>
</head>

<body>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Ảnh</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">
					Trang chủ</a></li>
			<li class="breadcrumb-item active">Ảnh</li>
		</ol>
		<div align="center">
			<h1>${title}</h1>
			<form:form action="/NguoiNghienShop/admin/saveImage" method="post"
				modelAttribute="image">
				<table>
					<form:hidden path="id" />
					<tr>
						<td>Danh sách giày:</td>
						<td><form:input path="id_product" required="Điền tên" /></td>
					</tr>
					<tr>
						<td>Link ảnh:</td>
						<td><form:input path="link" required="Điền link" /></td>
					</tr>

					<tr>
						<td colspan="2" align="center"><input type="submit"
							value="Save"></td>
						<td><button
								onclick="document.location.href ='/NguoiNghienShop/admin/image';"
								class="w3-button w3-black">Quay lại</button></td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
</body>
