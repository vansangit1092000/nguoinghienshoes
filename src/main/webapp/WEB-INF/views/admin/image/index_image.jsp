<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<title>Quản lý hình</title>
<style>
.image {
	height: 220px;
	width: 370px;
}
</style>


<main>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Hình</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">Trang
					chủ</a></li>
			<li class="breadcrumb-item active">Hình</li>
		</ol>

		<div class="card mb-4">

			<div class="card-body">
				<button
					onclick="document.location.href ='/NguoiNghienShop/admin/newImage';"
					class="w3-button w3-black">Thêm mới</button>
				<br> <br>
				<table id="datatablesSimple">
					<thead>
						<tr style="font-size: larger">
							<th>STT</th>
							<th>Tên sản phẩm</th>
							<th>Link Ảnh</th>
							<th>Chỉnh sửa</th>
						</tr>
					</thead>
					<tfoot>
						<tr style="font-size: larger">
							<th>STT</th>
							<th>Tên sản phẩm</th>
							<th>Link Ảnh</th>
							<th>Chỉnh sửa</th>
						</tr>
					</tfoot>
					<tbody>
						<c:forEach var="item" items="${image}" varStatus="status">
							<tr style="font-size: larger;">
								<td>${status.index + 1}</td>
								<td>
								<c:forEach var="item_pr" items="${product}">
									<c:if test="${item_pr.id_product == item.id_product }">
										${item_pr.name}-
											${item_pr.colors}
										
									</c:if>
								</c:forEach>
								</td>
								<td><img class="image"
											src="<c:url value="/assets/img/${item.link}"/>" /></td>
								<td>	
									<button
										onclick="document.location.href ='/NguoiNghienShop/admin/editImage?id=${item.id}';"
										class="w3-button w3-black">Chỉnh sửa</button>
									<button
										onclick="document.location.href ='/NguoiNghienShop/admin/deleteImage?id=${item.id}';"
										class="w3-button w3-black">Xóa</button>
										</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>

