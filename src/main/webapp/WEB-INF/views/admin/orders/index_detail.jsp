<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<title>Quản lý đơn hàng</title>

<style>
.image {
	height: 100px;
	width: 100px;
}
</style>

<main>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Chi tiết đơn hàng</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">Trang
					chủ</a></li>
			<li class="breadcrumb-item active">Chi tiết đơn hàng</li>
		</ol>

		<div class="card mb-4">
			<div class="card-body">
				<button
					onclick="document.location.href ='/NguoiNghienShop/admin/orders';"
					class="w3-button w3-black">Back</button>
					<br>
					<br>
					
				<table id="datatablesSimple">
					<thead>
						<tr style="font-size: larger;">
							<th>STT</th>
							<th>Tên sản phẩm</th>
							<th>Hình ảnh</th>
							<th>Size</th>
							<th>Giá tiền</th>
							<th>Số lượng</th>
							<th>Tổng tiền</th>
						</tr>
					</thead>
					<tfoot>
						<tr style="font-size: larger;">
							<th>STT</th>
							<th>Tên sản phẩm</th>
							<th>Hình ảnh</th>
							<th>Size</th>
							<th>Giá tiền</th>
							<th>Số lượng</th>
							<th>Tổng tiền</th>
						</tr>
					</tfoot>
					<tbody>

						<c:forEach var="item" items="${detail}" varStatus="status">
							<tr style="font-size: larger;">

								<td>${status.index + 1}</td>
								<c:forEach var="item_pr" items="${lstproduct}">
									<c:if test="${item_pr.id_product == item.id_product }">
										<td style="text-align: center;">${item_pr.name}-
											${item_pr.colors}</td>
										<td><img class="image"
											src="<c:url value="/assets/img/${item_pr.link}"/>" /></td>
									</c:if>
								</c:forEach>

								<td>${item.size}</td>
								<td><fmt:formatNumber type="number" groupingUsed="true"
										value="${item.price}" /> ₫</td>

								<td>${item.quantity}</td>
								<td><fmt:formatNumber type="number" groupingUsed="true"
										value="${item.amount}" /> ₫</td>

							</tr>

						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>

