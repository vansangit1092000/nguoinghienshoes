<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<head>
<title>Chỉnh sửa</title>
</head>

<body>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Đơn hàng</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">
					Trang chủ</a></li>
			<li class="breadcrumb-item active">Đơn hàng</li>
		</ol>
		<div align="center">
			<h1>${title}</h1>
			<form:form action="/NguoiNghienShop/admin/saveOrder" method="post"
				modelAttribute="order">
				<table>
					<form:hidden path="id" />
					<form:hidden path="customer_id" />
					<form:hidden path="total_bill" />
					<form:hidden path="payment_method" />
					<tr>
						<td>Ngày đặt:</td>
						<td><form:input path="created_date" required="Điền ngày" /></td>
					</tr>
					<tr>
						<td>Họ tên người nhận:</td>
						<td><form:input path="ship_name" required="Điền tên" /></td>
					</tr>
					<tr>
						<td>Số điện thoại:</td>
						<td><form:input path="ship_mobile" required="Điền sđt" /></td>
					</tr>
					<tr>
						<td>Địa chỉ:</td>
						<td><form:input path="ship_address" required="Điền địa chỉ" /></td>
					</tr>
					<tr>
						<td>Email:</td>
						<td><form:input path="ship_email" required="Điền email" /></td>
					</tr>
					<tr>
						<td>Trạng thái:</td>
						<td><form:select path="status" required="Chọn trạng thái">
							<form:option value="true">Đã xử lý</form:option>
							<form:option value="false">Chưa xử lý</form:option>
							</form:select>
							
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center"><input type="submit"
							value="Save"></td>
						<td><button
								onclick="document.location.href ='/NguoiNghienShop/admin/orders';"
								class="w3-button w3-black">Quay lại</button></td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
</body>
