<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<title>Quản lý đơn hàng</title>
<style>
.image {
	height: 150px;
	width: 300px;
}
</style>

<main>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Đơn hàng</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">Trang
					chủ</a></li>
			<li class="breadcrumb-item active">Đơn hàng</li>
		</ol>

		<div class="card mb-4">

			<div class="card-body">
				<table id="datatablesSimple">
					<thead>
						<tr>
							<th>STT</th>
							<th>Ngày đặt</th>
							<th>ID KH</th>
							<th>Tên</th>
							<th>Điện thoại</th>
							<th>Địa chỉ</th>
							<th>Email</th>
							<th>Phương thức thanh toán</th>
							<th>Tổng tiền</th>
							<th>Trạng thái</th>
							<th>Chỉnh sửa</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>STT</th>
							<th>Ngày đặt</th>
							<th>ID Khách hàng</th>
							<th>Tên</th>
							<th>Điện thoại</th>
							<th>Địa chỉ</th>
							<th>Email</th>
							<th>Phương thức thanh toán</th>
							<th>Tổng tiền</th>
							<th>Trạng thái</th>
							<th>Chỉnh sửa</th>
						</tr>
					</tfoot>
					<tbody>
						<c:forEach var="item" items="${orders}" varStatus="status">
							<tr style="font-size: larger;">
								<td>${status.index + 1}</td>
								<td>${item.created_date}</td>
								<td>${item.customer_id}</td>
								<td>${item.ship_name}</td>
								<td>${item.ship_mobile}</td>
								<td>${item.ship_address}</td>
								<td>${item.ship_email}</td>
								<td>${item.payment_method}</td>
								<td><fmt:formatNumber type="number" groupingUsed="true"
										value="${item.total_bill}" /> ₫</td>
								<c:set  var = "status" value = " "/>
								<c:if test="${item.status == true}">
									<c:set  var = "status" value = "Đã xử lý"/>
								</c:if>
								<c:if test="${item.status == false}">
									<c:set  var = "status" value = "Chưa xử lý"/>
								</c:if>
								<td>${status}</td>
								<td>
									<button
										onclick="document.location.href ='/NguoiNghienShop/admin/viewDetailOrder?id=${item.id}';"
										class="w3-button w3-black">Xem chi tiết</button>
										<br>
									<button
										onclick="document.location.href ='/NguoiNghienShop/admin/editOrder?id=${item.id}';"
										class="w3-button w3-black">Chỉnh sửa</button>
										<br>
									<button
										onclick="document.location.href ='/NguoiNghienShop/admin/deleteOrder?id=${item.id}';"
										class="w3-button w3-black">Xóa</button>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>

