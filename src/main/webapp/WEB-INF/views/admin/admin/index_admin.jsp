<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<title>Quản lý Admin</title>

<main>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Admin</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">Trang chủ</a></li>
			<li class="breadcrumb-item active">Admin</li>
		</ol>
	
		<div class="card mb-4">
			
			<div class="card-body">
			<button onclick="document.location.href ='/NguoiNghienShop/admin/newAdmin';" class="w3-button w3-black">
							Thêm mới
			</button>
			<br>
			<br>
			
				<table id="datatablesSimple">
					<thead>
						<tr style="font-size: larger;">
							<th>STT</th>
							<th>Họ và tên</th>
							<th>Tài khoản</th>
							<th>Mật khẩu</th>
							<th>Số điện thoại</th>
							<th>Chức vụ</th>						
							<th>Chỉnh sửa</th>
						</tr>
					</thead>
					<tfoot>
						<tr style="font-size: larger;">
							<th>STT</th>
							<th>Họ và tên</th>
							<th>Tài khoản</th>
							<th>Mật khẩu</th>
							<th>Số điện thoại</th>
							<th>Chức vụ</th>						
							<th>Chỉnh sửa</th>
						</tr>
					</tfoot>
					<tbody>
					<c:forEach var="item" items="${admin}" varStatus="status">				
						<tr style="font-size: larger;">
 							<td>${status.index + 1}</td>
							<td>${item.fullname}</td>
							<td>${item.username}</td>
							<td>${item.password}</td>
							<td>${item.phone}</td>
							<c:forEach var="item_role" items="${role}">
								<c:if test="${item.role_id == item_role.id}">
								<td>${item_role.role_name}</td></c:if>
							</c:forEach>
							<td>
							<button onclick="document.location.href ='/NguoiNghienShop/admin/editAdmin?id=${item.id}';" class="w3-button w3-black">
							Chỉnh sửa 
							</button>
							<button onclick="document.location.href ='/NguoiNghienShop/admin/deleteAdmin?id=${item.id}';" class="w3-button w3-black">
							Xóa
							</button>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>

