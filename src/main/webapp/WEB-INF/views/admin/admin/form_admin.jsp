<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<head>
<title>${title}</title>
</head>

<body>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Admin</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">
					Trang chủ</a></li>
			<li class="breadcrumb-item active">Admin</li>
		</ol>
		<div align="center">
			<h1>${title}</h1>
			<form:form action="/NguoiNghienShop/admin/saveAdmin" method="post"
				modelAttribute="admin">
				<table>
					<form:hidden path="id" />
					<tr>
						<td>Họ và tên:</td>
						<td><form:input path="fullname" required="Điền tên" /></td>
					</tr>
					<tr>
						<td>Tài khoản:</td>
						<td><form:input path="username" required="Điền Username" /></td>
					</tr>
					<tr>
						<td>Mật khẩu:</td>
						<td><form:input path="password" required="Điền Password" /></td>
					</tr>
					<tr>
						<td>Số điện thoại:</td>
						<td><form:input path="phone" required="Điền link" /></td>
					</tr>
					<tr>
						<td>Chức vụ:</td>
						<td><form:select path="role_id" required="Chọn trạng thái">
								<c:forEach var="item_role" items="${role}">
									<form:option value="${item_role.id}">${item_role.role_name }</form:option>
								</c:forEach>
							</form:select></td>
					</tr>
					<tr>
						<td colspan="2" align="center"><input type="submit"
							value="Save"></td>
						<td><button
								onclick="document.location.href ='/NguoiNghienShop/admin/admin';"
								class="w3-button w3-black">Quay lại</button></td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
</body>
