<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<head>
<title>Chỉnh sửa</title>
</head>

<body>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Người dùng</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">
					Trang chủ</a></li>
			<li class="breadcrumb-item active">Người dùng</li>
		</ol>
		<div align="center">
			<h1>${title}</h1>
			<form:form action="/NguoiNghienShop/admin/saveUser" method="post"
				modelAttribute="user">
				<table>
					<form:hidden path="id" />
					<form:hidden path="created_at" />
					<tr>
						<td>Tên đăng nhập:</td>
						<td><form:input type="text" path="username" required="Điền tên đăng nhập" /></td>
					</tr>
					<tr>
						<td>Mật khẩu:</td>
						<td><form:input type="password" path="password" required="Điền mật khẩu" /></td>
					</tr>
					<tr>
						<td>Họ và tên:</td>
						<td><form:input type="text" path="fullname" required="Điền tên" /></td>
					</tr>
					<tr>
						<td>Địa chỉ:</td>
						<td><form:input type="text" path="address" required="Điền địa chỉ" /></td>
					</tr>
					<tr>
						<td>Số điện thoại:</td>
						<td><form:input type="text" path="phone" required="Điền số điện thoại" /></td>
					</tr>
					<tr>
						<td>Email:</td>
						<td><form:input type="email" path="email" required="Điền email" /></td>						
					</tr>
					<tr>
						<td colspan="2" align="center"><input type="submit"
							value="Save"></td>
						<td><button
								onclick="document.location.href ='/NguoiNghienShop/admin/user';"
								class="w3-button w3-black">Quay lại</button></td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
</body>
