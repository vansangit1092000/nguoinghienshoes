<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layout/user/taglib.jsp"%>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<title>Quản lý người dùng</title>

<main>
	<div class="container-fluid px-4">
		<h1 class="mt-4">Người dùng</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item"><a href="<c:url value="/admin"/>">Trang chủ</a></li>
			<li class="breadcrumb-item active">Người dùng</li>
		</ol>
	
		<div class="card mb-4">
			
			<div class="card-body">
			
				<table id="datatablesSimple">
					<thead>
						<tr style="font-size: larger;">
							<th>STT</th>
							<th>Tên đăng nhập</th>
							<th>Họ và tên</th>
							<th>Địa chỉ</th>	
							<th>Số điện thoại</th>
							<th>Email</th>					
							<th>Chỉnh sửa</th>
						</tr>
					</thead>
					<tfoot>
						<tr style="font-size: larger;">
							<th>STT</th>
							<th>Tên đăng nhập</th>
							<th>Họ và tên</th>
							<th>Địa chỉ</th>	
							<th>Số điện thoại</th>
							<th>Email</th>					
							<th>Chỉnh sửa</th>
						</tr>
					</tfoot>
					<tbody>
					<c:forEach var="item" items="${user}" varStatus="status">				
						<tr style="font-size: larger;">
 							<td>${status.index + 1}</td>
							<td>${item.username}</td>
							<td>${item.fullname}</td>
							<td>${item.address}</td>
							<td>${item.phone}</td>
							<td>${item.email}</td>
							<td>
							<button onclick="document.location.href ='/NguoiNghienShop/admin/editUser?id=${item.id}';" class="w3-button w3-black">
							Chỉnh sửa 
							</button>
							<button onclick="document.location.href ='/NguoiNghienShop/admin/deleteUser?id=${item.id}';" class="w3-button w3-black">
							Xóa
							</button>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>

