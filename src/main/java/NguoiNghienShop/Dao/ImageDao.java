package NguoiNghienShop.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import NguoiNghienShop.Dto.ProductDto;
import NguoiNghienShop.Entity.Images;
import NguoiNghienShop.Entity.MapperImage;

@Repository
public class ImageDao extends BaseDao {

	public List<Images> GetAllImages() {
		List<Images> list = new ArrayList<Images>();
		String sql = "SELECT * FROM image ORDER BY id DESC";
		list = _jdbcTemplate.query(sql, new MapperImage());
		return list;
	}

//	// Insert Product
//	public void AddImage(Images ima) {
//		String sql = "INSERT INTO `image` (`id`, `id_product`, `link`) "
//				+ "VALUES (NULL, '" + ima.getId_product() + "', '" + ima.getLink() +"'"; 
//		_jdbcTemplate.update(sql);
//	}
//	
//	// Insert Product
//		public void UpdateImage(Images ima) {
//			String sql = "UPDATE `image` set id_product=?, link=? WHERE id=? ";
//			_jdbcTemplate.update(sql,ima.getId_product(),ima.getLink(),ima.getId());
//	}

	public Images GetIDImage(int id) {
		String sql = "SELECT * FROM image WHERE id=" + id;
		Images ima = _jdbcTemplate.queryForObject(sql, new MapperImage());
		return ima;
	}

	public void DeleteImage(int id) {
		String sql = "DELETE FROM image WHERE id=?";
		_jdbcTemplate.update(sql, id);
	}

	public void saveOrUpdateImage(Images ima) {

			if (ima.getId() > 0) {
				// update
				String sql = "UPDATE `image` set id_product=?, link=? WHERE id=?";
				_jdbcTemplate.update(sql, ima.getId_product(), ima.getLink(), ima.getId());
			} else {
				// insert
				String sql = "INSERT INTO `image` (`id_product`, `link`) " + "VALUES ('" + ima.getId_product()
						+ "', '" + ima.getLink() + "')";
				_jdbcTemplate.update(sql);
			}
			
	}
}