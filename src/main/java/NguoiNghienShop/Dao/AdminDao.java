package NguoiNghienShop.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import NguoiNghienShop.Entity.Admin;
import NguoiNghienShop.Entity.MapperAdmin;
import NguoiNghienShop.Entity.MapperRole;
import NguoiNghienShop.Entity.Role;



@Repository
public class AdminDao extends BaseDao {
	
	public Admin GetAccount(Admin admin) {

		String sql = "SELECT * FROM admin WHERE username = '" + admin.getUsername() + "', password = '"+ admin.getUsername()+"'";
		Admin check = _jdbcTemplate.queryForObject(sql, new MapperAdmin());

		if (check == null) {
			return null;
		} else {
			return check;
		}
	};
	
	public List<Admin> GetAllAdmin() {
		List<Admin> list = new ArrayList<Admin>();
		String sql = "SELECT * FROM admin ORDER BY id DESC";
		list = _jdbcTemplate.query(sql, new MapperAdmin());
		return list;
	}
	
	public List<Role> GetAllRole() {
		List<Role> list = new ArrayList<Role>();
		String sql = "SELECT * FROM role";
		list = _jdbcTemplate.query(sql, new MapperRole());
		return list;
	}
	
	public Admin GetIDAdmin(int id) {
		String sql = "SELECT * FROM admin WHERE id=" + id;
		Admin ad = _jdbcTemplate.queryForObject(sql, new MapperAdmin());
		return ad;
	}

	public void DeleteAdmin(int id) {
		String sql = "DELETE FROM image WHERE id=?";
		_jdbcTemplate.update(sql, id);
	}

	public void saveOrUpdateAdmin(Admin ad) {
			if (ad.getId() > 0) {
				// update
				String sql = "UPDATE `admin` set fullname=?, username=?, password=?, phone=?, role_id=?  WHERE id=?";
				_jdbcTemplate.update(sql, ad.getFullname(), ad.getUsername(), ad.getPassword() , ad.getPhone(), ad.getRole_id(), ad.getId());
			} else {
				// insert
				String sql = "INSERT INTO `admin` (`fullname`, `username`, `password`, `phone`, `role_id`) " + "VALUES ('" + ad.getFullname()
						+ "', '" + ad.getUsername() + "', '" +ad.getPassword()+"', '"+ad.getPhone()+"', '"+ad.getRole_id()+"')";
				_jdbcTemplate.update(sql);
			}
			
	}

}
