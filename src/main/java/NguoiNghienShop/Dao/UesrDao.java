package NguoiNghienShop.Dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import NguoiNghienShop.Entity.MapperUser;
import NguoiNghienShop.Entity.User;

@Repository
public class UesrDao extends BaseDao {

	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Calendar cal = Calendar.getInstance();

	public User GetAccount(User user) {

		String sql = "SELECT * FROM user WHERE username = " + "'" + user.getUsername() + "'";
		User check = _jdbcTemplate.queryForObject(sql, new MapperUser());

		if (check == null) {
			return null;
		} else {
			return check;
		}
	};

	public int InsertAccount(User user) {
		String sql = "INSERT INTO `user` "
				+ "(`username`, `fullname`, `password`, `address`, `phone`, `email`, `created_at`, `updated_at`) "
				+ "VALUES ('" + user.getUsername() + "', '" + user.getFullname() + "', '" + user.getPassword() + "', '"
				+ user.getAddress() + "','" + user.getPhone() + "', '" + user.getEmail() + "', '"
				+ dateFormat.format(cal.getTime()) + "' ,'" + dateFormat.format(cal.getTime()) + "')";
		int insert = _jdbcTemplate.update(sql);
		return insert;
	};

	public List<User> GetAllUser() {
		List<User> listuser = new ArrayList<User>();
		String sql = "SELECT * FROM user";
		listuser = _jdbcTemplate.query(sql, new MapperUser());
		return listuser;
	}

	public User FindUserById(int id) {
		User user = new User();
		String sql = "SELECT * FROM user WHERE id = " + id;
		user = _jdbcTemplate.queryForObject(sql, new MapperUser());
		return user;
	}

	public void saveOrUpdateUser(User user) {
		String sql = "UPDATE user SET username='" + user.getUsername() + "', fullname='" + user.getFullname()
				+ "', password='" + user.getPassword() + "'" + ", address='" + user.getAddress() + "', phone='"
				+ user.getPhone() + "', email='" + user.getEmail() + "'" + ", created_at='" + user.getCreated_at()
				+ "', updated_at='" + dateFormat.format(cal.getTime()) + "'" + "WHERE id=" + user.getId();
		_jdbcTemplate.update(sql);
	}

	public void deleteUser(int id) {
		String sql = "DELETE FROM user WHERE id=?";
		_jdbcTemplate.update(sql, id);
	}

}
