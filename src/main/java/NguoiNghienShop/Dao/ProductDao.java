package NguoiNghienShop.Dao;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import NguoiNghienShop.Dto.ProductDto;
import NguoiNghienShop.Dto.ProductDtoMapper;



@Repository
public class ProductDao extends BaseDao{
	//Sql lay toan bo san pham
	private String SqlString() {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("p.id as id_product, ");
		sql.append("p.id_category, ");
		sql.append("p.name, ");
		sql.append("p.colors, ");
		sql.append("p.price, ");
		sql.append("p.sale, ");
		sql.append("p.title, ");
		sql.append("p.highlight, ");
		sql.append("p.new_product, ");
		sql.append("p.detail, ");
		sql.append("p.created_at, ");
		sql.append("p.updated_at, ");
		sql.append("i.link ");
		sql.append("FROM products as p, image i ");
		sql.append("WHERE p.id = i.id_product ");
		sql.append("GROUP BY p.id");
		return sql.toString();
	}
		
	//sql lay san pham theo danh muc
	private String SqlProductByCate(int id) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT p.id as id_product, p.id_category, p.name, p.colors, p.price, p.sale, p.title, p.highlight, p.new_product, p.detail, p.created_at, p.updated_at, i.link FROM products as p, image i "
				+ "WHERE p.id = i.id_product and id_category = "
				+id+" GROUP BY p.id ");
		return sql.toString();
		
	}
	
	//sql lay san pham theo phan trang
	private String SqlProductByPaginates(int start, int totalPage) {
		StringBuffer sql = new StringBuffer();
		start = start - 1;
		sql.append("SELECT p.id as id_product, p.id_category, p.name, p.colors, p.price, p.sale, p.title, p.highlight, p.new_product, p.detail, p.created_at, p.updated_at, i.link FROM products as p, image i "
				+ "WHERE p.id = i.id_product "
				+" GROUP BY p.id LIMIT "+start + ", " + totalPage);
		return sql.toString();
		
	}
	
	//sql lay san pham theo phan trang theo danh muc
		private String SqlProductByPaginatesCate(int start, int totalPage, int id) {
			StringBuffer sql = new StringBuffer();
			start = start - 1;
			sql.append("SELECT p.id as id_product, p.id_category, p.name, p.colors, p.price, p.sale, p.title, p.highlight, p.new_product, p.detail, p.created_at, p.updated_at, i.link FROM products as p, image i "
					+ "WHERE p.id = i.id_product and id_category = "
					+id
					+" GROUP BY p.id LIMIT "+start + ", " + totalPage);
			return sql.toString();
			
		}
		
		//sql lay san pham theo phan trang theo tim kiem
				private String SqlProductByPaginatesSearch(int start, int totalPage, String name) {
					StringBuffer sql = new StringBuffer();
					start = start - 1;
					sql.append("SELECT p.id as id_product, p.id_category, p.name, p.colors, p.price, p.sale, p.title, p.highlight, p.new_product, p.detail, p.created_at, p.updated_at, i.link FROM products as p, image i "
							+ "WHERE p.id = i.id_product and p.name LIKE N'%"
							+ name + "%'"
							+" GROUP BY p.id LIMIT "+start + ", " + totalPage);
					return sql.toString();
					
				}
		
		
		
		//----------------------------END SQL-------------
		
	
	//Lay tat ca san pham
	public List<ProductDto> GetDataProduct(){
		List<ProductDto> list = new ArrayList<ProductDto>();
		String sql = SqlString();
		list = _jdbcTemplate.query(sql, new ProductDtoMapper());
		return list;
	}
	
	
	//Lay tat ca san pham co phan trang theo danh mục
	public List<ProductDto> GetDataProductPaginates(int id,int start, int totalPage){
		String sqlGetDataByID = SqlProductByCate(id);
		List<ProductDto> listproductCate = _jdbcTemplate.query(sqlGetDataByID, new ProductDtoMapper());
		List<ProductDto> list = new ArrayList<ProductDto>();
		if(listproductCate.size() > 0) {
			String sql = SqlProductByPaginatesCate(start,totalPage,id);
			list = _jdbcTemplate.query(sql, new ProductDtoMapper());
		}	
		return list;
	}
	
	//Lay tat ca san pham co phan trang theo tim kiem
		public List<ProductDto> GetDataProductPaginatesss(String name,int start, int totalPage){
			List<ProductDto> listsearch = FindName(name);
			List<ProductDto> list = new ArrayList<ProductDto>();
			if(listsearch.size() > 0) {
				String sql = SqlProductByPaginatesSearch(start,totalPage,name);
				list = _jdbcTemplate.query(sql, new ProductDtoMapper());
			}	
			return list;
		}
		
	
	
	
	//Lay tat ca san pham co phan trang theo san pham
	public List<ProductDto> GetDataProductPaginatess(int start, int totalPage){
		String sqlGetData = SqlString();
		List<ProductDto> listproduct = _jdbcTemplate.query(sqlGetData, new ProductDtoMapper());
		List<ProductDto> list = new ArrayList<ProductDto>();
		if(listproduct.size() > 0) {
			String sql = SqlProductByPaginates(start,totalPage);
			list = _jdbcTemplate.query(sql, new ProductDtoMapper());
		}	
		return list;
	}
	
	//lay top 3 san pham noi bat
	public List<ProductDto> Get3ProductFeatured(){
		List<ProductDto> list = new ArrayList<ProductDto>();
		String sql1 = "SELECT p.id as id_product, p.id_category, p.name, p.colors, p.price, p.sale, p.title, p.highlight, p.new_product, p.detail, p.created_at, p.updated_at, i.link FROM products as p, image i WHERE p.id = i.id_product and p.highlight = true GROUP BY p.id ORDER BY RAND() LIMIT 3";
		list = _jdbcTemplate.query(sql1, new ProductDtoMapper());
		return list;
	}
	
	//lay top 3 san pham moi
	public List<ProductDto> Get3ProductNew(){
		List<ProductDto> list = new ArrayList<ProductDto>();
		String sql2 = "SELECT p.id as id_product, p.id_category, p.name, p.colors, p.price, p.sale, p.title, p.highlight, p.new_product, p.detail, p.created_at, p.updated_at, i.link FROM products as p, image i WHERE p.id = i.id_product and p.new_product = true GROUP BY p.id ORDER BY RAND() LIMIT 3";
		list = _jdbcTemplate.query(sql2, new ProductDtoMapper());		
		return list;
	}
	
	//lay san pham theo danh muc
	public List<ProductDto> GetProductByCate(int id){
		List<ProductDto> list = new ArrayList<ProductDto>();
		String sql = "SELECT p.id as id_product, p.id_category, p.name, p.colors, p.price, p.sale, p.title, p.highlight, p.new_product, p.detail, p.created_at, p.updated_at, i.link FROM products as p, image i WHERE p.id = i.id_product and id_category = "
				+ id
				+ " GROUP by p.id";	
		list = _jdbcTemplate.query(sql, new ProductDtoMapper());
		return list;
	}
	
	//Lay san pham theo ID
	public List<ProductDto> GetProductById(long id){
		List<ProductDto> list = new ArrayList<ProductDto>();
		String sql = "SELECT p.id as id_product, p.id_category, p.name, p.colors, p.price, p.sale, p.title, p.highlight, p.new_product, p.detail, p.created_at, p.updated_at, i.link FROM products as p, image i WHERE p.id = i.id_product and p.id = "
				+ id
				+ " LIMIT 1 ";	
		list = _jdbcTemplate.query(sql, new ProductDtoMapper());
		return list;
	}
	
	public ProductDto FindProductById(long id){
		ProductDto product = new ProductDto();
		String sql = "SELECT p.id as id_product, p.id_category, p.name, p.colors, p.price, p.sale, p.title, p.highlight, p.new_product, p.detail, p.created_at, p.updated_at, i.link FROM products as p, image i WHERE p.id = i.id_product and p.id = "
				+ id
				+ " LIMIT 1 ";
		product = _jdbcTemplate.queryForObject(sql, new ProductDtoMapper());
		return product;
	}
	
	//Lay hinh cua san pham
	public List<ProductDto> GetImgById(long id){
		List<ProductDto> list = new ArrayList<ProductDto>();
		String sql = "SELECT p.id as id_product, p.id_category, p.name, p.colors, p.price, p.sale, p.title, p.highlight, p.new_product, p.detail, p.created_at, p.updated_at, i.link FROM products as p, image i WHERE p.id = i.id_product and p.id = "
				+ id
				;	
		list = _jdbcTemplate.query(sql, new ProductDtoMapper());
		return list;
	}
	
	
	//lay size va so luong cua san pham
	public List<ProductDto> GetSizeById(long id){
		List<ProductDto> list = new ArrayList<ProductDto>();
		String sql = "SELECT DISTINCT dts.id_product,s.id,s.name_size, dts.quantity FROM sizes s, detailsize dts WHERE s.id = dts.id_size and id_product = "
				+ id
				+ " ORDER BY id_product";	
		list = _jdbcTemplate.query(sql, new ProductDtoMapper());
		return list;
	}
	
	//Tim kiem theo ten san pham
	public List<ProductDto> FindName(String name){
		List<ProductDto> list = new ArrayList<ProductDto>();

		String sql = "SELECT p.id as id_product, p.id_category, p.name, p.colors, p.price, p.sale, p.title, p.highlight, p.new_product, p.detail, p.created_at, p.updated_at, i.link FROM products as p, image i "
				+ "WHERE p.id = i.id_product and p.name LIKE N'%"
				+ name + "%' GROUP by p.id";			
		list = _jdbcTemplate.query(sql, new ProductDtoMapper());
		return list;
	}
	
//	//Insert Product
//	public void AddProduct(ProductDto product){
//		String sql = "INSERT INTO `products` (`id`, `id_category`, `name`, `colors`, `sale`, `title`, `highlight`, `new_product`, `detail`, `price`, `created_at`, `updated_at`) "
//				+ "VALUES (NULL, '"+product.getId_category()+"', '"+product.getName()+"', '"+product.getColors()+"', '"+product.getSale()+"', '"+product.getTitle()+"', '"+product.isHighlight()+"', '"+product.isNew_product()+"', '"+product.getDetail()+"', '"+product.getPrice()+"', '"+dateFormat.format(cal.getTime())+"', '"+dateFormat.format(cal.getTime())+"')";
//		 _jdbcTemplate.update(sql);
//	
//	}
//	
//	//Edit Product
//		public void EditProduct(long id){
//			ProductDto product = FindProductById(id);
//			String sql = "UPDATE `products` set id_category=?, name=?, colors=?, sale=?, title=?, highlight=?, new_product=?, detail=?, price=?, created_at=?, updated_at=? WHERE id=?) ";
//					//+ "VALUES (NULL, '"+product.getId_category()+"', '"+product.getName()+"', '"+product.getColors()+"', '"+product.getSale()+"', '"+product.getTitle()+"', '"+product.isHighlight()+"', '"+product.isNew_product()+"', '"+product.getDetail()+"', '"+product.getPrice()+"', '"+dateFormat.format(cal.getTime())+"', '"+dateFormat.format(cal.getTime())+"')";
//			_jdbcTemplate.update(sql, product.getId_category(), product.getName(), product.getColors(), product.getSale(), product.getTitle(), product.isHighlight(), product.isNew_product(),  product.getDetail(), product.getPrice(), product.getCreated_at(), dateFormat.format(cal.getTime()), product.getId_product());
//		}
		
	
	
	//ADMIN
	//Delete Product
		public void DeleteProduct(long id) {
			 String sql = "DELETE FROM products WHERE id=?";
			    _jdbcTemplate.update(sql, id);
		}
		
		
		public void saveOrUpdateProduct(ProductDto product) {		
			int sale = 0;
			if(product.getSale() == 1) sale= 1;
			int highlight = 0;
			if(product.isHighlight() == true) sale= 1;
			int new_product = 0;
			if(product.isNew_product() == true) sale= 1;
			
		    if (product.getId_product() > 0) {
		        // update
		        String sql = "UPDATE `products` set id_category=?, name=?, colors=?, sale=?, title=?, highlight=?, new_product=?, detail=?, price=?, updated_at=? WHERE id=? ";
		        _jdbcTemplate.update(sql, product.getId_category(), product.getName(), product.getColors(),sale, product.getTitle(), highlight,new_product,  product.getDetail(), product.getPrice(), product.getCreated_at(), dateFormat.format(cal.getTime()), product.getId_product());
		    } else {
		        // insert
		        String sql = "INSERT INTO `products` (`id`, `id_category`, `name`, `colors`, `sale`, `title`, `highlight`, `new_product`, `detail`, `price`, `created_at`, `updated_at`) "
						+ "VALUES (NULL, '"+product.getId_category()+"', '"+product.getName()+"', '"+product.getColors()+"', '"+sale+"', '"+product.getTitle()+"', '"+highlight+"', '"+new_product+"', '"+product.getDetail()+"', '"+product.getPrice()+"', '"+dateFormat.format(cal.getTime())+"', '"+dateFormat.format(cal.getTime())+"')";
				 _jdbcTemplate.update(sql);
		    }
		}
		

		
		
		
}
