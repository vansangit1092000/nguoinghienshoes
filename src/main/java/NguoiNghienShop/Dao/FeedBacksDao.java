package NguoiNghienShop.Dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.springframework.stereotype.Repository;

import NguoiNghienShop.Entity.FeedBacks;

@Repository
public class FeedBacksDao extends BaseDao {
	
	public int AddFeedBacks(FeedBacks feedback) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		
		String sql = "INSERT INTO `feedback` "
				+ "(`name`, `phone`, `email`, `address`, `message`, `created_at`, `status`) "
				+ "VALUES ('"+feedback.getName()+"', '"+feedback.getPhone()+"', '"+feedback.getEmail()+"', '"
				+feedback.getAddress()+"','"+feedback.getMessage()+"', '"+dateFormat.format(cal.getTime())+"', '0')";
		int insert =  _jdbcTemplate.update(sql);
		return insert;
					
	};
}
