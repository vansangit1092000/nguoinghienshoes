package NguoiNghienShop.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import NguoiNghienShop.Entity.MapperSlides;
import NguoiNghienShop.Entity.Slides;

@Repository
public class SlidesDao extends BaseDao {

	
	public List<Slides> GetDataSlide(){
		List<Slides> list = new ArrayList<Slides>();
		String sql = "SELECT * FROM slides";
		list = _jdbcTemplate.query(sql, new MapperSlides());
		return list;
	}
	
	public Slides FindSlidesById(int id){
		Slides slides = new Slides();
		String sql = "SELECT * FROM slides WHERE id = "
				+ id;
		slides = _jdbcTemplate.queryForObject(sql, new MapperSlides());
		return slides;
	}
	
	public void saveOrUpdateSlides(Slides slides) {
	    if (slides.getId() > 0) {
	        // update
	        String sql = "UPDATE slides SET img='"+slides.getImg()+"', caption='"+slides.getCaption()+"' "
	                    + "WHERE id="+slides.getId();
	        _jdbcTemplate.update(sql);
	    } else {
	        // insert
	        String sql = "INSERT INTO slides (img, caption)"
	                    + " VALUES ('"+slides.getImg()+"','"+slides.getCaption()+"')";
	        _jdbcTemplate.update(sql);
	    }
	}
	
	public void deleteSlides(int id) {
	    String sql = "DELETE FROM slides WHERE id=?";
	    _jdbcTemplate.update(sql, id);
	}
	
}
