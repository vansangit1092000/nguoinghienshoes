package NguoiNghienShop.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import NguoiNghienShop.Entity.Images;
import NguoiNghienShop.Entity.MapperSizes;
import NguoiNghienShop.Entity.Sizes;

@Repository
public class SizesDao {
	
	@Autowired
	public JdbcTemplate jdbc;
	
	public List<Sizes> GetSizeById(long id){
		List<Sizes> list = new ArrayList<Sizes>();
		String sql = "SELECT DISTINCT dts.id_product,s.id,s.name_size, dts.quantity FROM sizes s, detailsize dts WHERE s.id = dts.id_size and id_product = "
				+ id
				+ " and quantity > 0 ORDER BY id_product,s.name_size";	
		list = jdbc.query(sql, new MapperSizes());
		return list;
	}
	
	public List<Sizes> GetAllSize(){
		List<Sizes> list = new ArrayList<Sizes>();
		String sql = "SELECT DISTINCT dts.id_product,s.id,s.name_size, dts.quantity FROM sizes s, detailsize dts WHERE s.id = dts.id_size and "
				+ " quantity > 0 ORDER BY id_product,s.name_size";	
		list = jdbc.query(sql, new MapperSizes());
		return list;
	}
	
	// Insert Product
		public int AddDetaisSize(Sizes sizes) {
			String sql = "INSERT INTO `detailsize` "
					+ "(`id_product`, `id_size`, `quantity') "
					+ "VALUES ('"+sizes.getId_product()+"', '"+sizes.getId()+"', '"+sizes.getQuantity()+"'";
			int insert = jdbc.update(sql);		
			return insert;
		}
		
		// Insert Product
			public void UpdateDetaisSize(Sizes sizes) {
				String sql = "UPDATE `image` set id_size=?, quantity=? WHERE id=? ";
				jdbc.update(sql,sizes.getId_product(),sizes.getQuantity(),sizes.getId());
		}

}
