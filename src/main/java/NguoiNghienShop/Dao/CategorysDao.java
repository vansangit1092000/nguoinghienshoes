package NguoiNghienShop.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import NguoiNghienShop.Entity.Categorys;
import NguoiNghienShop.Entity.MapperCategorys;

@Repository
public class CategorysDao extends BaseDao {
	
	
	public List<Categorys> GetDataCategorys(){
		List<Categorys> list = new ArrayList<Categorys>();
		String sql = "SELECT * FROM category LIMIT 3";
		list = _jdbcTemplate.query(sql, new MapperCategorys());
		return list;
	}
	
	public List<Categorys> GetAllCategorys(){
		List<Categorys> list = new ArrayList<Categorys>();
		String sql = "SELECT * FROM category";
		list = _jdbcTemplate.query(sql, new MapperCategorys());
		return list;
	}
	
	public Categorys FindCateById(int id){
		Categorys cate = new Categorys();
		String sql = "SELECT * FROM category WHERE id = "
				+ id;
		cate = _jdbcTemplate.queryForObject(sql, new MapperCategorys());
		return cate;
	}
	
	public void saveOrUpdateCate(Categorys cate) {
	    if (cate.getId() > 0) {
	        // update
	        String sql = "UPDATE category SET name='"+cate.getName()+"', mota='"+cate.getMota()+"', img='"+cate.getImg()+"'"
	                    + "WHERE id="+cate.getId();
	        _jdbcTemplate.update(sql);
	    } else {
	        // insert
	        String sql = "INSERT INTO category (name, mota, img)"
	                    + " VALUES ('"+cate.getName()+"','"+cate.getMota()+"', '"+cate.getImg()+"')";
	        _jdbcTemplate.update(sql);
	    }
	}
	
	public void deleteCate(int id) {
	    String sql = "DELETE FROM category WHERE id=?";
	    _jdbcTemplate.update(sql, id);
	}
	
	
}
