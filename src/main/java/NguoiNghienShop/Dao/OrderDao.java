package NguoiNghienShop.Dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import NguoiNghienShop.Dto.OrderDetailDto;
import NguoiNghienShop.Dto.OrderDetailDtoMapper;
import NguoiNghienShop.Entity.Categorys;
import NguoiNghienShop.Entity.MapperCategorys;
import NguoiNghienShop.Entity.MapperOrderDetails;
import NguoiNghienShop.Entity.MapperOrders;
import NguoiNghienShop.Entity.OrderDetails;
import NguoiNghienShop.Entity.Orders;

@Repository
public class OrderDao extends BaseDao {
	public int AddOrders(Orders orders) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		
		String sql = "INSERT INTO `orders` "
				+ "(`created_date`, `customer_id`, `ship_name`, `ship_mobile`, `ship_address`, `ship_email`, `payment_method`, `status`, `total_bill`) "
				+ "VALUES ('"+dateFormat.format(cal.getTime())+"', "+orders.getCustomer_id()+", '"+orders.getShip_name()+"', '"
				+orders.getShip_mobile()+"','"+orders.getShip_address()+"', '"+orders.getShip_email()+"', '"+orders.getPayment_method()+"' , 0, "+orders.getTotal_bill()+")";
		int insert =  _jdbcTemplate.update(sql);
		return insert;
							
	};
	
	public int GetIDLastOrders() {
		String  sql = new String();
		sql = "SELECT * FROM orders WHERE id = (SELECT MAX(id) FROM orders)";
		Orders order =  _jdbcTemplate.queryForObject(sql, new MapperOrders());
		return order.getId();
	}
	
	public int AddDetailsOrders(OrderDetails orderDetails) {		
		String sql = "INSERT INTO `orderdetails` "
				+ "(`id_product`, `id_order`, `size`, `price`, `quantity`, `amount`) "
				+ "VALUES ('"+orderDetails.getId_product()+"', '"+orderDetails.getId_order()+"', '"+orderDetails.getSize()+"', '"
				+orderDetails.getPrice()+"','"+orderDetails.getQuantity()+"', '"+orderDetails.getAmount()+"')";
		int insert =  _jdbcTemplate.update(sql);
		return insert;							
	}
	
	
	public List<OrderDetails> GetDetailsOrder(int id) {
		List<OrderDetails> list = new ArrayList<OrderDetails>();
		String  sql = "Select * from orderdetails WHERE id_order= " +id+";";
		list =  _jdbcTemplate.query(sql, new MapperOrderDetails());
		return list;
	}
	
	public List<Orders> GetAllOrders(){
		List<Orders> list = new ArrayList<Orders>();
		String sql = "SELECT * FROM orders";
		list = _jdbcTemplate.query(sql, new MapperOrders());
		return list;
	}
	
	public Orders FindOrderById(int id){
		Orders order = new Orders();
		String sql = "SELECT * FROM orders WHERE id = "
				+ id;
		order = _jdbcTemplate.queryForObject(sql, new MapperOrders());
		return order;
	}
	
	public void saveOrUpdateOrder(Orders order) {
	        // update
			int status = 0;
			if(order.isStatus() == true) status= 1;
	        String sql = "UPDATE orders SET created_date='"+order.getCreated_date()+"', customer_id='"+order.getCustomer_id()+"', ship_name='"+order.getShip_name()+"'"
	        			+ ", ship_mobile='" +order.getShip_mobile()+ "', ship_email='" +order.getShip_email()
	        			+"', payment_method='"+order.getPayment_method()+"', status="+status
	        			+ ", total_bill="+order.getTotal_bill() 
	                    +" WHERE id="+order.getId();
	        _jdbcTemplate.update(sql);

	}
	
	public void deleteOrder(int id) {
	    String sql = "DELETE FROM orders WHERE id=?";
	    _jdbcTemplate.update(sql, id);
	}
	
}

