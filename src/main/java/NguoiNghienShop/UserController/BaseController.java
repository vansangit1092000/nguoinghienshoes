package NguoiNghienShop.UserController;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import NguoiNghienShop.Service.User.CategoryServiceImpl;
import NguoiNghienShop.Service.User.HomeServiceImpl;

@Controller
public class BaseController {
	@Autowired
	HomeServiceImpl _homeService;
	
	@Autowired
	CategoryServiceImpl categoryService;

	public ModelAndView _mvShare = new ModelAndView();

	@PostConstruct
	public ModelAndView Init() {
		_mvShare.addObject("menus", _homeService.GetDataMenus());
		_mvShare.addObject("categorys", categoryService.GetAllCate());
		return _mvShare;
	}
	
	

}
