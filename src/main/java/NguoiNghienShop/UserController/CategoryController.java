package NguoiNghienShop.UserController;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import NguoiNghienShop.Dto.PaginatesDto;
import NguoiNghienShop.Service.User.CategoryServiceImpl;
import NguoiNghienShop.Service.User.HomeServiceImpl;
import NguoiNghienShop.Service.User.PaginatesServiceImpl;

@Controller
public class CategoryController extends BaseController {
	@Autowired
	private CategoryServiceImpl categoryService;
	
	@Autowired
	private HomeServiceImpl homeService;
	
	@Autowired
	private PaginatesServiceImpl  paginatesService;
	
	private int totalProductsPage = 9;
	
	@RequestMapping(value = "/san-pham/{id}")
	public ModelAndView ProductByCate(@PathVariable String id) {
		_mvShare.setViewName("user/product/index_product");
		
		int totalData = categoryService.GetProductsByCate(Integer.parseInt(id)).size();
		_mvShare.addObject("totalData", totalData);
		PaginatesDto paginateInfo = paginatesService.GetInfoPaginates(totalData, totalProductsPage, 1);
		_mvShare.addObject("idCategory", id);
		_mvShare.addObject("paginateInfo", paginateInfo);
		_mvShare.addObject("productsPaginates", categoryService.GetDataProductsPaginates(Integer.parseInt(id),paginateInfo.getStart(), totalProductsPage));
		return _mvShare;
	}
	
	@RequestMapping(value = "/san-pham/{id}/{currentPage}")
	public ModelAndView ProductByCate(@PathVariable String id, @PathVariable String currentPage ) {
		_mvShare.setViewName("user/product/index_product");
		
		int totalData = categoryService.GetProductsByCate(Integer.parseInt(id)).size();
		PaginatesDto paginateInfo = paginatesService.GetInfoPaginates(totalData, totalProductsPage, Integer.parseInt(currentPage));
		
		_mvShare.addObject("idCategory", id);
		_mvShare.addObject("paginateInfo", paginateInfo);
		_mvShare.addObject("productsPaginates", categoryService.GetDataProductsPaginates(Integer.parseInt(id),paginateInfo.getStart(), totalProductsPage));
		return _mvShare;
	}
	
	@RequestMapping(value = "/san-pham")
	public ModelAndView Product() {
		_mvShare.setViewName("user/product/index");
		_mvShare.addObject("products", homeService.GetAllProduct());
		int totalData = categoryService.GetAllProducts().size();
		PaginatesDto paginateInfo = paginatesService.GetInfoPaginates(totalData, totalProductsPage, 1);
		
		_mvShare.addObject("paginateInfo", paginateInfo);
		_mvShare.addObject("productsPaginates", categoryService.GetDataProductsPaginatess(paginateInfo.getStart(), totalProductsPage));
		return _mvShare;
	}
	
	@RequestMapping(value = "/san-pham-all/{currentPage}")
	public ModelAndView Product(@PathVariable String currentPage ) {
		_mvShare.setViewName("user/product/index");
		
		int totalData = categoryService.GetAllProducts().size();
		PaginatesDto paginateInfo = paginatesService.GetInfoPaginates(totalData, totalProductsPage, Integer.parseInt(currentPage));
		
		_mvShare.addObject("paginateInfo", paginateInfo);
		_mvShare.addObject("productsPaginates", categoryService.GetDataProductsPaginatess(paginateInfo.getStart(), totalProductsPage));
		return _mvShare;
		
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView Find(HttpServletRequest rq ) {
		_mvShare.setViewName("user/product/search");	
		String name = rq.getParameter("name");
		int totalData = categoryService.FindName(name).size();
		PaginatesDto paginateInfo = paginatesService.GetInfoPaginates(totalData, totalProductsPage,1);
		_mvShare.addObject("name", name);
		_mvShare.addObject("paginateInfo", paginateInfo);
		_mvShare.addObject("productsPaginates", categoryService.GetDataProductsPaginatesss(name,paginateInfo.getStart(), totalProductsPage));
		return _mvShare;
		
	}
	
	
	@RequestMapping(value = "/search/{name}/{currentPage}")
	public ModelAndView Find(@PathVariable String name, @PathVariable String currentPage ) {
		_mvShare.setViewName("user/product/search");
		
		int totalData = categoryService.FindName(name).size();
		PaginatesDto paginateInfo = paginatesService.GetInfoPaginates(totalData, totalProductsPage, Integer.parseInt(currentPage));
		
		_mvShare.addObject("name", name);
		_mvShare.addObject("paginateInfo", paginateInfo);
		_mvShare.addObject("productsPaginates", categoryService.GetDataProductsPaginatesss(name,paginateInfo.getStart(), totalProductsPage));
		return _mvShare;
	}
	
}
