package NguoiNghienShop.UserController;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import NguoiNghienShop.Dto.CartDto;
import NguoiNghienShop.Entity.Orders;
import NguoiNghienShop.Entity.User;
import NguoiNghienShop.Service.User.CartServiceImpl;
import NguoiNghienShop.Service.User.OrdersServiceImpl;

@Controller
public class CartController extends BaseController {

	@Autowired 
	private CartServiceImpl cartService = new CartServiceImpl();
	
	@Autowired 
	private OrdersServiceImpl ordersService = new OrdersServiceImpl();
	
	@RequestMapping(value = "gio-hang")
	public ModelAndView Index(HttpSession session) {	
		HashMap<Long, CartDto> cart = (HashMap<Long, CartDto>)session.getAttribute("Cart"); 
		if(cart == null || cart.values().isEmpty()) {
			_mvShare.setViewName("user/cart/NoCart");
		}
		_mvShare.setViewName("user/cart/list_cart");		
		return _mvShare;
	}
	
	@RequestMapping(value = "thanh-cong")
	public ModelAndView Index() {			
		_mvShare.setViewName("user/cart/success");		
		return _mvShare;
	}
	
	
	@RequestMapping(value = "AddCart/{id}")
	public String AddCart(HttpServletRequest request,HttpSession session, @PathVariable long id) {
		
		HashMap<Long, CartDto> cart = (HashMap<Long, CartDto>)session.getAttribute("Cart"); 
		if(cart == null) {
			cart = new HashMap<Long, CartDto>();
		}
		cart = cartService.AddCart(id, cart);
		session.setAttribute("Cart", cart);
		session.setAttribute("TotalQuanty", cartService.TotalQuanty(cart) ); 
		session.setAttribute("TotalPrice", cartService.TotalPrice(cart) );
		return "redirect:"+request.getHeader("Referer");
	}
	
	@RequestMapping(value = "EditCart/{id}/{quanty}")
		public String EditCart(HttpServletRequest request,HttpSession session, @PathVariable long id, @PathVariable int quanty) {
		
		HashMap<Long, CartDto> cart = (HashMap<Long, CartDto>)session.getAttribute("Cart"); 
		if(cart == null) {
			cart = new HashMap<Long, CartDto>();
		}
		cart = cartService.EditCart(id, quanty ,cart);
		session.setAttribute("Cart", cart);
		session.setAttribute("TotalQuanty", cartService.TotalQuanty(cart) );
		session.setAttribute("TotalPrice", cartService.TotalPrice(cart) );
		return "redirect:"+request.getHeader("Referer");
	}
	
	@RequestMapping(value = "DeleteCart/{id}")
	public String DeleteCart(HttpServletRequest request,HttpSession session, @PathVariable long id) {
		HashMap<Long, CartDto> cart = (HashMap<Long, CartDto>)session.getAttribute("Cart"); 
		if(cart == null) {
			cart = new HashMap<Long, CartDto>();
		}
		
		cart = cartService.DeleteCart(id, cart);
		session.setAttribute("Cart", cart);
		session.setAttribute("TotalQuanty", cartService.TotalQuanty(cart) );
		session.setAttribute("TotalPrice", cartService.TotalPrice(cart) );
		return "redirect:"+request.getHeader("Referer");
	}
	
	@RequestMapping(value = "gio-hang-rong")
	public ModelAndView DeleteAll(HttpSession session) {
		HashMap<Long, CartDto> cart = (HashMap<Long, CartDto>)session.getAttribute("Cart"); 
		if(cart == null) {
			cart = new HashMap<Long, CartDto>();
		}
		
		cart = cartService.DeleteAll( cart);
		session.removeAttribute("Cart");
		session.removeAttribute("TotalQuanty");
		_mvShare.setViewName("user/cart/NoCart");		
		return _mvShare;
	}
	
	@RequestMapping(value = "thanh-toan", method = RequestMethod.GET)
	public ModelAndView Checkout(HttpServletRequest request,HttpSession session) {
		_mvShare.setViewName("user/cart/checkout");	
		Orders orders = new Orders();
		User loginInfor = (User)session.getAttribute("LoginInfor");
		if(loginInfor != null) {			
			orders.setShip_name(loginInfor.getFullname());
			orders.setShip_email(loginInfor.getEmail());
			orders.setShip_address(loginInfor.getAddress());
			orders.setShip_mobile(loginInfor.getPhone());
		}
		_mvShare.addObject("orders", orders);
		
		return _mvShare;
	}
	
	@RequestMapping(value = "thanh-toan", method = RequestMethod.POST)
	public String CheckoutBill(HttpServletRequest request,HttpSession session, @ModelAttribute("orders") Orders orders) {
		
		
		try {
			User loginInfor = (User)session.getAttribute("LoginInfor");
			orders.setPayment_method("Thanh toán khi nhận hàng");
			orders.setCustomer_id(loginInfor.getId());
			orders.setTotal_bill((Double)session.getAttribute("TotalPrice"));
			if(ordersService.AddOrder(orders) > 0) {
				HashMap<Long, CartDto> carts = (HashMap<Long, CartDto>)session.getAttribute("Cart");
				ordersService.AddDetailsOrders(carts);
			}
			session.removeAttribute("Cart");
			session.removeAttribute("TotalPrice");
			session.removeAttribute("TotalQuanty");
			_mvShare.addObject("order", orders);
			return "redirect:/thanh-cong";
		} catch (Exception e) {
			return "redirect:/khong-tim-thay";
		}
		
	}
}
