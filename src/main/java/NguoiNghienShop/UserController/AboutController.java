package NguoiNghienShop.UserController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AboutController extends BaseController {

	@RequestMapping(value = "/gioi-thieu")
	public ModelAndView About() {
		_mvShare.setViewName("user/index_about");		
		return _mvShare;
	}
}
