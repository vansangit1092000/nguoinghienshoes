package NguoiNghienShop.UserController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import NguoiNghienShop.Entity.FeedBacks;
import NguoiNghienShop.Service.User.IFeedBackService;


@Controller
public class FeedBacksController extends BaseController{
	
	@Autowired
	private IFeedBackService feedBackService;
	
	@RequestMapping(value = "/lien-he", method = RequestMethod.GET)
	public ModelAndView Show() {
		_mvShare.setViewName("user/index_lienhe");	
		_mvShare.addObject("feedbacks", new FeedBacks());
		return _mvShare;
	}
	
	@RequestMapping(value = "/lien-he", method = RequestMethod.POST)
	public ModelAndView SaveDB(@ModelAttribute("feedbacks") FeedBacks feedbacks) {
		int count = feedBackService.AddFeedBacks(feedbacks);
		if(count > 0) {
			_mvShare.addObject("status", "Cảm ơn bạn đã góp ý !!! Chúng mình đã nhận được thông tin ^^");
			 _mvShare.setViewName("user/index_lienhe");
		}
		else {
			_mvShare.addObject("status", "Liên hệ thất bại rồi! Bạn hãy thử lại nhé ");
			_mvShare.setViewName("user/index_lienhe");
		}

		return _mvShare;
	}
}
