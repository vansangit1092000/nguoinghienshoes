package NguoiNghienShop.UserController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import NguoiNghienShop.Entity.User;
import NguoiNghienShop.Service.User.IUserService;

@Controller
public class UserController extends BaseController{
	
	@Autowired
	private IUserService _userService;
	
	@RequestMapping(value = "/dang-ky", method = RequestMethod.GET)
	public ModelAndView DangKy() {
		_mvShare.setViewName("user/account/register");
		_mvShare.addObject("user", new User());
		return _mvShare;
	}
	
	@RequestMapping(value = "/dang-ky", method = RequestMethod.POST)
	public ModelAndView CreateAcc(@ModelAttribute("user") User user) {
		int count = _userService.InsertAccount(user);
		if(count > 0) {
			 _mvShare.setViewName("redirect:/dang-nhap");
		}
		else {
			_mvShare.addObject("status", "Đăng ký tài khoản thất bại! ");
			_mvShare.setViewName("user/account/register");
		}

		return _mvShare;
	}
	
	@RequestMapping(value = "/dang-nhap", method = RequestMethod.POST)
	public ModelAndView CheckLogin(HttpSession session, HttpServletRequest request,@ModelAttribute("user") User user) {
		
		if(user.getUsername() == "") {
			_mvShare.addObject("Statuslogin", "Vui lòng điền đủ thông tin");
			return _mvShare;
		}
		
		User check = _userService.CheckAccount(user);
		if (check != null) {
			 session.setAttribute("LoginInfor", check);
			 _mvShare.setViewName("redirect:/trang-chu");
		} else {
			_mvShare.addObject("Statuslogin", "Tài khoản hoặc mật khẩu sai !");
		}
		return _mvShare;	

	}
	@RequestMapping(value = "/dang-nhap", method = RequestMethod.GET)
	public ModelAndView DangNhap() {
		User user = new User();
		_mvShare.addObject(user);
		_mvShare.setViewName("user/account/login");
		return _mvShare;
	}
	
	@RequestMapping(value = "/dang-xuat", method = RequestMethod.GET)
	public String Login(HttpSession session, HttpServletRequest request) {
		session.removeAttribute("LoginInfor");
		return "redirect:"+request.getHeader("Referer");
	}
	
	
}
