package NguoiNghienShop.UserController;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class HomeController extends BaseController{


	@RequestMapping(value = {"/", "/trang-chu"})
	public ModelAndView Index() {	
		_mvShare.addObject("slides", _homeService.GetDataSlide());
		_mvShare.addObject("category", _homeService.GetDataCategory());
		_mvShare.addObject("productpf", _homeService.Get3ProductFeatured());
		_mvShare.addObject("productpn", _homeService.Get3ProductNew());
		_mvShare.setViewName("user/index");		
		return _mvShare;
	}
	
	@RequestMapping(value = {"/khong-tim-thay"})
	public ModelAndView NoPage() {	
		_mvShare.setViewName("user/pagenotfound");		
		return _mvShare;
	}
	
	
		
	
}
