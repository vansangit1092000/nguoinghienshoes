package NguoiNghienShop.UserController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import NguoiNghienShop.Service.User.ICategoryService;
import NguoiNghienShop.Service.User.IProductService;
import NguoiNghienShop.Service.User.ISizesService;

@Controller
public class ProductController extends BaseController {
	
	@Autowired
	private IProductService _productService;
	
	@Autowired
	private ISizesService sizesService;
	
	@Autowired
	private ICategoryService categoryService;
	
	@RequestMapping(value = "chi-tiet-san-pham/{id}")
	public ModelAndView Index(@PathVariable long id) {
		_mvShare.addObject("product", _productService.GetProductById(id));
		_mvShare.addObject("img", _productService.GetImgById(id));
		_mvShare.addObject("sizes", sizesService.GetSizeById(id));
		int id_cate = _productService.GetProductById(id).getId_category();
		_mvShare.addObject("productcate", _productService.GetProductByCate(id_cate));
		_mvShare.addObject("cate", categoryService.FindCateById(id_cate));
		_mvShare.setViewName("user/product/detail");
		return _mvShare;
	}
}
