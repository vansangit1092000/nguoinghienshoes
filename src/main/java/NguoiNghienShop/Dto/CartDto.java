package NguoiNghienShop.Dto;

import lombok.Data;

@Data
public class CartDto {
	private int quanty;
	private double totalPrice;
	private ProductDto product;
}
