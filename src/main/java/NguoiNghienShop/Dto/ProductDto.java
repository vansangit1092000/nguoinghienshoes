package NguoiNghienShop.Dto;

import java.sql.Date;

import lombok.Data;

@Data
public class ProductDto {
	private long id_product;
	private int id_category;
	private String name;
	private String colors;
	private double price;
	private int sale;
	private String title;
	private boolean highlight;
	private boolean new_product;
	private String detail;
	private Date created_at;
	private Date updated_at;
	private String link;
	
	
}
