package NguoiNghienShop.Dto;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class OrderDetailDtoMapper implements RowMapper<OrderDetailDto> {
	public OrderDetailDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		OrderDetailDto orDetail = new OrderDetailDto();
		orDetail.setName(rs.getString("name"));
		orDetail.setSize(rs.getString("size"));
		orDetail.setPrice(rs.getDouble("price"));
		orDetail.setQuantity(rs.getInt("quantity"));
		orDetail.setAmount(rs.getDouble("amount"));
		
		return orDetail;
	}

}
