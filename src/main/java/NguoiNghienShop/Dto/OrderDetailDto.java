package NguoiNghienShop.Dto;

import lombok.Data;

@Data
public class OrderDetailDto {
	
	public String name;
	public String size;
	public double price;
	public int quantity;
	public double amount;
}
