package NguoiNghienShop.Dto;

import NguoiNghienShop.Entity.Images;
import NguoiNghienShop.Entity.Sizes;
import lombok.Data;

@Data
public class ProductAdminDto {
	private ProductDto product;
	private Images image;
	private Sizes  size;
}
