package NguoiNghienShop.Dto;

import lombok.Data;

@Data
public class PaginatesDto {
	private int currentPage;
	private int limit;
	private int start;
	private int end;
	private int totalPage;
}
