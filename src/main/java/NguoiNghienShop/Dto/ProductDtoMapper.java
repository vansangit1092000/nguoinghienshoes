package NguoiNghienShop.Dto;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


public class ProductDtoMapper implements RowMapper<ProductDto> {
	public ProductDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProductDto products = new ProductDto();
		products.setId_product(rs.getLong("id_product"));
		products.setId_category(rs.getInt("id_category"));
		products.setName(rs.getString("name"));
		products.setColors(rs.getString("colors"));
		products.setPrice(rs.getDouble("price"));
		products.setSale(rs.getInt("sale"));
		products.setTitle(rs.getString("title"));
		products.setHighlight(rs.getBoolean("highlight"));
		products.setNew_product(rs.getBoolean("new_product"));
		products.setDetail(rs.getString("detail"));
		products.setCreated_at(rs.getDate("created_at"));
		products.setUpdated_at(rs.getDate("updated_at"));
		products.setLink(rs.getString("link"));
		return products;
	}
}
