package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.stereotype.Service;

import NguoiNghienShop.Dto.ProductDto;

@Service
public interface IProductService {

	public ProductDto GetProductById(long id);
	
	public List<ProductDto> GetImgById(long id);
	
	public List<ProductDto> GetProductByCate(int id);
	
	public List<ProductDto> GetAllProducts();

	public void DeleteProduct(long id);
	public void saveOrUpdateProduct(ProductDto product);
	

}
