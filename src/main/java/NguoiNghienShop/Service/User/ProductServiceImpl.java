package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import NguoiNghienShop.Dao.ProductDao;
import NguoiNghienShop.Dto.ProductDto;

@Service
public class ProductServiceImpl implements IProductService{
	
	@Autowired
	ProductDao productDao = new ProductDao();
	
	
	public ProductDto GetProductById(long id) {
		List<ProductDto> list = productDao.GetProductById(id);
		return list.get(0);
	}

	public List<ProductDto> GetImgById(long id) {
		return productDao.GetImgById(id);
	}

	public List<ProductDto> GetProductByCate(int id) {
		return productDao.GetProductByCate(id);
	}

	public List<ProductDto> GetAllProducts() {
		
		return productDao.GetDataProduct();
	}


	public void DeleteProduct(long id) {
		productDao.DeleteProduct(id);
		
	}

	public void saveOrUpdateProduct(ProductDto product) {
		productDao.saveOrUpdateProduct(product);
		
	}

	



}
