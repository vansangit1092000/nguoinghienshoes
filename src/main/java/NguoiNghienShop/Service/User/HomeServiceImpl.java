package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import NguoiNghienShop.Dao.CategorysDao;
import NguoiNghienShop.Dao.MenuDao;
import NguoiNghienShop.Dao.ProductDao;
import NguoiNghienShop.Dao.SlidesDao;
import NguoiNghienShop.Dto.ProductDto;
import NguoiNghienShop.Entity.Categorys;
import NguoiNghienShop.Entity.Menus;
import NguoiNghienShop.Entity.Slides;

@Service
public class HomeServiceImpl implements IHomeService{
	@Autowired
	private SlidesDao slidesDao;
	@Autowired
	private CategorysDao categorysDao;
	@Autowired
	private MenuDao menuDao;
	@Autowired
	private ProductDao productDao;

	
	public List<Slides> GetDataSlide() {
		
		return slidesDao.GetDataSlide();
	}

	public List<Categorys> GetDataCategory(){
		return categorysDao.GetDataCategorys();
	};
	
	public List<Menus> GetDataMenus(){
		return menuDao.GetDataMenus();
	};

	public List<ProductDto> GetAllProduct() {
		List<ProductDto> listproduct = productDao.GetDataProduct();
		return listproduct;
	};
	
	public List<ProductDto> Get3ProductFeatured() {
		List<ProductDto> listpt = productDao.Get3ProductFeatured();
		return listpt;
	};
	
	public List<ProductDto> Get3ProductNew() {
		List<ProductDto> listpn = productDao.Get3ProductNew();
		return listpn;
	};
	
	

}
