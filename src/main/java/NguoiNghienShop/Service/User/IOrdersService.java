package NguoiNghienShop.Service.User;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;

import NguoiNghienShop.Dto.CartDto;
import NguoiNghienShop.Dto.OrderDetailDto;
import NguoiNghienShop.Entity.OrderDetails;
import NguoiNghienShop.Entity.Orders;

@Service
public interface IOrdersService  {
	public int AddOrder(Orders orders);
	public void AddDetailsOrders(HashMap<Long, CartDto> carts);
	public List<Orders> GetAllOrders();
	public Orders FindOrderById(int id);
	public void saveOrUpdateOrder(Orders order);
	public void deleteOrder(int id);
	public List<OrderDetails> GetDetailsOrder(int id);
}
