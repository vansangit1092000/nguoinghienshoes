package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import NguoiNghienShop.Dto.ProductDto;
import NguoiNghienShop.Entity.Categorys;
import NguoiNghienShop.Entity.Menus;
import NguoiNghienShop.Entity.Slides;

@Service
public interface IHomeService {

	public List<Slides> GetDataSlide();
	public List<Categorys>  GetDataCategory();
	public List<Menus>  GetDataMenus();
	public List<ProductDto>  GetAllProduct();
	public List<ProductDto> Get3ProductFeatured();
	public List<ProductDto> Get3ProductNew();
}
