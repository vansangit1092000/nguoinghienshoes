package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import NguoiNghienShop.Dao.ImageDao;
import NguoiNghienShop.Entity.Images;

@Service
public class ImageServiceImpl implements IImageService {

	@Autowired
	private ImageDao imagedao;
	
	public List<Images> GetAllImg() {
		
		return imagedao.GetAllImages();
	}

	public void DeleteImage(int id) {
		imagedao.DeleteImage(id);
		
	}

	public void saveOrUpdateImage(Images ima) {
		imagedao.saveOrUpdateImage(ima);
		
	}

	public Images GetIDImage(int id) {
		return imagedao.GetIDImage(id);
	}

}
