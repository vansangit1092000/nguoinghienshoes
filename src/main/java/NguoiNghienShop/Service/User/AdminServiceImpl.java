package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import NguoiNghienShop.Dao.AdminDao;
import NguoiNghienShop.Entity.Admin;
import NguoiNghienShop.Entity.Role;

@Service
public class AdminServiceImpl implements IAdminService {

	@Autowired AdminDao adminDao;
	public List<Admin> GetAllAdmin() {
		
		return adminDao.GetAllAdmin();
	}

	public Admin GetIDAdmin(int id) {
		// TODO Auto-generated method stub
		return adminDao.GetIDAdmin(id);
	}

	public void DeleteAdmin(int id) {
		// TODO Auto-generated method stub
		adminDao.DeleteAdmin(id);
		
	}

	public void saveOrUpdateAdmin(Admin ad) {
		// TODO Auto-generated method stub
		adminDao.saveOrUpdateAdmin(ad);
		
	}

	public List<Role> GetAllRole() {
		// TODO Auto-generated method stub
		return adminDao.GetAllRole();
	}

	public Admin GetAccount(Admin admin) {
		// TODO Auto-generated method stub
		return adminDao.GetAccount(admin);
	}

}
