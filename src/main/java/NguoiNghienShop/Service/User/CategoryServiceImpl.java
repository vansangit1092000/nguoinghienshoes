package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import NguoiNghienShop.Dao.CategorysDao;
import NguoiNghienShop.Dao.ProductDao;
import NguoiNghienShop.Dto.ProductDto;
import NguoiNghienShop.Entity.Categorys;
@Service
public class CategoryServiceImpl implements ICategoryService{
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private CategorysDao categoryDao;
	
	
	public List<ProductDto> GetProductsByCate(int id) {
		List<ProductDto> products =  productDao.GetProductByCate(id);
		return products;
	}
	
	public List<ProductDto> GetDataProductsPaginates(int id, int start, int totalPage) {
		List<ProductDto> products =  productDao.GetDataProductPaginates(id, start, totalPage);
		return products;
	}
	
	public List<ProductDto> GetDataProductsPaginatesss(String name, int start, int totalPage) {
		List<ProductDto> products =  productDao.GetDataProductPaginatesss(name, start, totalPage);
		return products;
	}
	
	
	public List<ProductDto> GetDataProductsPaginatess(int start, int totalPage) {
		List<ProductDto> products =  productDao.GetDataProductPaginatess(start, totalPage);
		return products;
	}
	
	public List<Categorys> GetAllCate() {
		List<Categorys> cate =  categoryDao.GetAllCategorys();
		return cate;
	}
	
	public List<ProductDto> GetAllProducts() {
		List<ProductDto> product =  productDao.GetDataProduct();
		return product;
	}

	public Categorys FindCateById(int id) {	
		return categoryDao.FindCateById(id);
	}

	public void SaveOrUpdateCate(Categorys cate) {
		categoryDao.saveOrUpdateCate(cate);		
	}

	public void DeleteCate(int id) {
		categoryDao.deleteCate(id);				
	}

	public List<ProductDto> FindName(String name) {		
		return productDao.FindName(name);
	}


	

}
