package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import NguoiNghienShop.Dao.SizesDao;
import NguoiNghienShop.Entity.Sizes;

@Service
public class SizeServiceImpl implements ISizesService {

	@Autowired
	SizesDao sizesDao = new SizesDao();
	
	public List<Sizes> GetSizeById(long id) {
		
		return sizesDao.GetSizeById(id);
	}

	public List<Sizes> GetAllSize() {
		return sizesDao.GetAllSize();
	}

}
