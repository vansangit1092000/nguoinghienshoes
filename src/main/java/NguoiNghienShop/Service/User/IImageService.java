package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.stereotype.Service;

import NguoiNghienShop.Entity.Images;


@Service
public interface IImageService  {
	
	public List<Images> GetAllImg();
	
	public void DeleteImage(int id);
	public void saveOrUpdateImage(Images ima);
	public Images GetIDImage(int id);

	
	
}
