package NguoiNghienShop.Service.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import NguoiNghienShop.Dao.FeedBacksDao;
import NguoiNghienShop.Entity.FeedBacks;

@Service
public class FeedBackService implements IFeedBackService{

	@Autowired
	public FeedBacksDao feedBacksDao;
	
	public int AddFeedBacks(FeedBacks feedbacks) {
		
		return feedBacksDao.AddFeedBacks(feedbacks);
	}

}
