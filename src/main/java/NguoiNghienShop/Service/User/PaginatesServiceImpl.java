package NguoiNghienShop.Service.User;

import org.springframework.stereotype.Service;

import NguoiNghienShop.Dto.PaginatesDto;

@Service
public class PaginatesServiceImpl implements IPaginatesService {

	public PaginatesDto GetInfoPaginates(int totalData, int limit, int curentPage) {
		PaginatesDto paginates = new PaginatesDto();	
		paginates.setLimit(limit);
		
		int total = SetInfoTotalPage(totalData, limit); 
		paginates.setTotalPage(total);
		paginates.setCurrentPage(CheckCurrentPage(curentPage, total ));
		
		int start = FindStart(paginates.getCurrentPage(), limit);
		paginates.setStart(start);
		int end = FindEnd(paginates.getStart(), limit, totalData) ;
		paginates.setEnd(end);
		return paginates;
	}

	private int FindEnd(int start, int limit, int totalData) {
		
		return start + limit > totalData ? totalData : (start + limit)-1 ;
	}

	private int FindStart(int currentPage, int limit) {
		
		return ((currentPage - 1) * limit)+1;
	}

	private int SetInfoTotalPage(int totalData, int limit) {
		int totalPage = 0;
		totalPage = totalData / limit;
		totalPage = totalPage * limit < totalData ? totalPage + 1 : totalPage;
		return totalPage;		
	}

	private int CheckCurrentPage(int curentPage, int totalPage) { 
		if (curentPage < 1) {
			return 1;
		}
		if (curentPage > totalPage) {
			return totalPage;
		}
		return curentPage;
	}
	
}
