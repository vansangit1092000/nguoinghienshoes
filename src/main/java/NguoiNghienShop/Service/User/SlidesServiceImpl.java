package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import NguoiNghienShop.Dao.SlidesDao;
import NguoiNghienShop.Entity.Slides;

@Service
public class SlidesServiceImpl implements ISlidesService{

	
	@Autowired
	private SlidesDao slidesDao;
	
	public List<Slides> GetDataSlide() {
		return slidesDao.GetDataSlide();
	}

	public Slides FindSlidesById(int id) {
		return slidesDao.FindSlidesById(id);
	}

	public void SaveOrUpdateSlides(Slides slides) {
		slidesDao.saveOrUpdateSlides(slides);
		
	}

	public void DeleteSlides(int id) {
		slidesDao.deleteSlides(id);
		
	}

}
