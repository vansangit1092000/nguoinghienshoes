package NguoiNghienShop.Service.User;

import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import NguoiNghienShop.Dao.UesrDao;
import NguoiNghienShop.Entity.User;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	UesrDao userDao;
	
	public User CheckAccount(User user) {	
		String pass = user.getPassword();
		user =  userDao.GetAccount(user);
		if(user != null) {
			if(BCrypt.checkpw( pass,  user.getPassword())) {
				return user;
			}
			else {
				return null;
			}
			
		}
		return null;		
	}
	
	public int InsertAccount(User user) {
		user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(12)));
		
		return userDao.InsertAccount(user);
	}

	public User FindUserById(int id) {
		return userDao.FindUserById(id);
	}

	public void saveOrUpdateUser(User user) {
		user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(12)));
		userDao.saveOrUpdateUser(user);	
	}

	public void deleteUser(int id) {
		userDao.deleteUser(id);
		
	}
	
	public List<User> GetAllUser() {
		return userDao.GetAllUser();
		
	}

}
