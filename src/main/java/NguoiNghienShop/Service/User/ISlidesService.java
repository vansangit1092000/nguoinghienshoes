package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.stereotype.Service;

import NguoiNghienShop.Entity.Slides;

@Service
public interface ISlidesService {
	public List<Slides> GetDataSlide();
	
	public Slides FindSlidesById(int id);
	
	public void SaveOrUpdateSlides(Slides slides);
	
	public void DeleteSlides(int id);
	

}
