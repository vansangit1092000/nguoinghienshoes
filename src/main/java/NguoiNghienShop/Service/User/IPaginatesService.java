package NguoiNghienShop.Service.User;

import org.springframework.stereotype.Service;

import NguoiNghienShop.Dto.PaginatesDto;

@Service
public interface IPaginatesService {
	public PaginatesDto GetInfoPaginates(int totalData, int limit, int curentPage);
}
