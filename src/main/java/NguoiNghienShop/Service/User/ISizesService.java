package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.stereotype.Service;

import NguoiNghienShop.Entity.Sizes;

@Service
public interface ISizesService {
	public List<Sizes> GetSizeById(long id);
	public List<Sizes> GetAllSize();
	
}
