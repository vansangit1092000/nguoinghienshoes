package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.stereotype.Service;

import NguoiNghienShop.Entity.Admin;
import NguoiNghienShop.Entity.Role;

@Service
public interface IAdminService  {
	public List<Admin> GetAllAdmin();
	public Admin GetIDAdmin(int id);
	public void DeleteAdmin(int id);
	public void saveOrUpdateAdmin(Admin ad);
	public List<Role> GetAllRole();
	public Admin GetAccount(Admin admin);
}
