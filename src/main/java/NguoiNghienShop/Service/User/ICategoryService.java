package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.stereotype.Service;

import NguoiNghienShop.Dto.ProductDto;
import NguoiNghienShop.Entity.Categorys;

@Service
public interface ICategoryService {

	public List<ProductDto> GetDataProductsPaginates(int id, int start, int totalPage);
	
	public List<ProductDto> GetProductsByCate(int id);
	
	public List<Categorys> GetAllCate();
	
	public List<ProductDto> FindName(String name);
	
	public List<ProductDto> GetDataProductsPaginatesss(String name, int start, int totalPage);
	
	public Categorys FindCateById(int id);
	
	public void SaveOrUpdateCate(Categorys cate);
	
	public void DeleteCate(int id);
}
