package NguoiNghienShop.Service.User;

import java.util.List;

import org.springframework.stereotype.Service;

import NguoiNghienShop.Entity.User;

@Service
public interface IUserService {
	public User CheckAccount(User user);
	public int InsertAccount(User user);
	public User FindUserById(int id);
	public void saveOrUpdateUser(User user);
	public void deleteUser(int id);
	public List<User> GetAllUser();
}
