package NguoiNghienShop.Service.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import NguoiNghienShop.Dao.OrderDao;
import NguoiNghienShop.Dto.CartDto;
import NguoiNghienShop.Dto.OrderDetailDto;
import NguoiNghienShop.Entity.OrderDetails;
import NguoiNghienShop.Entity.Orders;

@Service
public class OrdersServiceImpl implements IOrdersService {
	@Autowired
	private OrderDao orderDao;
	
	public int AddOrder(Orders orders) {	
		return orderDao.AddOrders(orders);
	}

	public void AddDetailsOrders(HashMap<Long, CartDto> carts) {
		int idOrder = orderDao.GetIDLastOrders();
		
		for(Map.Entry<Long, CartDto> itemCart: carts.entrySet()) {
			OrderDetails orderDetails = new OrderDetails();
			orderDetails.setId_order(idOrder);
			orderDetails.setId_product(itemCart.getValue().getProduct().getId_product());
			orderDetails.setPrice(itemCart.getValue().getProduct().getPrice());
			orderDetails.setQuantity(itemCart.getValue().getQuanty());
			orderDetails.setAmount(itemCart.getValue().getTotalPrice());
			orderDetails.setSize("38");
			orderDao.AddDetailsOrders(orderDetails);
		}		
	}

	public List<Orders> GetAllOrders() {
		return orderDao.GetAllOrders();
	}

	public Orders FindOrderById(int id) {
		return orderDao.FindOrderById(id);
	}

	public void saveOrUpdateOrder(Orders order) {
		orderDao.saveOrUpdateOrder(order);
		
	}

	public void deleteOrder(int id) {
		orderDao.deleteOrder(id);		
	}

	public List<OrderDetails> GetDetailsOrder(int id) {
		return orderDao.GetDetailsOrder(id);
	}
	
}
