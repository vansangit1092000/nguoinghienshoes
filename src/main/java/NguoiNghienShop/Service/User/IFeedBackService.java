package NguoiNghienShop.Service.User;

import org.springframework.stereotype.Service;

import NguoiNghienShop.Entity.FeedBacks;

@Service
public interface IFeedBackService {
	public int AddFeedBacks(FeedBacks feedbacks);
}
