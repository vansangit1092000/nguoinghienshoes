package NguoiNghienShop.Entity;

import lombok.Data;

@Data
public class Sizes {
	private long id_product;
	private int id;
	private String name_size;
	private long quantity;
}
