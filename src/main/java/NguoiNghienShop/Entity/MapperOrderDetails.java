package NguoiNghienShop.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperOrderDetails implements RowMapper<OrderDetails> {
	
	public OrderDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
		OrderDetails orderdetails = new OrderDetails();
		orderdetails.setId(rs.getInt("id"));
		orderdetails.setId_product(rs.getLong("id_product"));
		orderdetails.setId_order(rs.getInt("id_order"));
		orderdetails.setSize(rs.getString("size"));
		orderdetails.setPrice(rs.getDouble("price"));
		orderdetails.setQuantity(rs.getInt("quantity"));
		orderdetails.setAmount(rs.getDouble("amount"));
		return orderdetails;
	}

}
