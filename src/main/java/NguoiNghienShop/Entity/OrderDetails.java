package NguoiNghienShop.Entity;

import lombok.Data;

@Data
public class OrderDetails {
	private int id;
	private long id_product;
	private int id_order;
	private String size;
	private double price;
	private int quantity;
	private double amount;

}
