package NguoiNghienShop.Entity;

import java.sql.Date;

import lombok.Data;

@Data
public class FeedBacks {
	private int id;
	private String name;
	private String phone;
	private String email;
	private String address;
	private String message;
	private Date created_at;
	private boolean status;
}
