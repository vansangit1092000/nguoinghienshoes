package NguoiNghienShop.Entity;

import lombok.Data;

@Data
public class Admin {
	private int id;
	private String fullname;
	private String username;
	private String password;
	private String phone;
	private int role_id;
}
