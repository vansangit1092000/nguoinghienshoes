package NguoiNghienShop.Entity;

import lombok.Data;

@Data
public class Slides {
	private int id;
	private String img;
	private String caption;
	
	
}
