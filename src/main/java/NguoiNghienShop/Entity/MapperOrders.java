package NguoiNghienShop.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperOrders implements RowMapper<Orders >{
	
	public Orders mapRow(ResultSet rs, int rowNum) throws SQLException {
	Orders orders = new Orders();
	orders.setId(rs.getInt("id"));
	orders.setCreated_date(rs.getDate("created_date"));
	orders.setCustomer_id(rs.getInt("customer_id"));
	orders.setShip_name(rs.getString("ship_name"));
	orders.setShip_mobile(rs.getString("ship_mobile"));
	orders.setShip_address(rs.getString("ship_address"));
	orders.setShip_email(rs.getString("ship_email"));
	orders.setPayment_method(rs.getString("payment_method"));
	orders.setStatus(rs.getBoolean("status"));
	orders.setTotal_bill(rs.getDouble("total_bill"));
	
	return orders;
	}
}
