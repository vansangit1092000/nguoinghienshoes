package NguoiNghienShop.Entity;

import lombok.Data;

@Data
public class Role {
	private int id;
	private String role_name;
}
