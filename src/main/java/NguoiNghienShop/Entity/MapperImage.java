package NguoiNghienShop.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperImage implements RowMapper<Images>{
	public Images mapRow(ResultSet rs, int rowNum) throws SQLException {
		Images img = new Images();
		img.setId(rs.getInt("id"));
		img.setId_product(rs.getLong("id_product"));
		img.setLink(rs.getString("link"));	
		return img;
	}
}
