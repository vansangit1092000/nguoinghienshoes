package NguoiNghienShop.Entity;

import java.sql.Date;

import lombok.Data;

@Data
public class Orders {
	private int id;
	private Date created_date;
	private int customer_id;
	private String ship_name;
	private String ship_mobile;
	private String ship_address;
	private String ship_email;
	private String payment_method;
	private boolean status;
	private double total_bill;
	
}
