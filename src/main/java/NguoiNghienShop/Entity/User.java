package NguoiNghienShop.Entity;

import java.sql.Date;

import lombok.Data;

@Data
public class User {
	private int id;
	private String username;
	private String fullname;
	private String password;
	private String address;
	private String phone;
	private String email;
	private Date created_at;
	private Date updated_at;
}
