package NguoiNghienShop.Entity;

import lombok.Data;

@Data
public class Images {
	private int id;  
	private long id_product;  
	private String link;  
}
