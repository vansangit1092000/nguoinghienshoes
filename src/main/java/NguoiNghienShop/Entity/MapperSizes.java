package NguoiNghienShop.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperSizes implements RowMapper<Sizes>{
	
	public Sizes mapRow(ResultSet rs, int rowNum) throws SQLException {
		Sizes sizes = new Sizes();
		sizes.setId_product(rs.getInt("id_product"));
		sizes.setId(rs.getInt("id"));
		sizes.setName_size(rs.getString("name_size"));
		sizes.setQuantity(rs.getInt("quantity"));
		return sizes;
	}
}
