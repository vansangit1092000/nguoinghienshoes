package NguoiNghienShop.Entity;

import lombok.Data;

@Data
public class Menus {
	
	private int id;
	private String name;
	private String url;
	
}
