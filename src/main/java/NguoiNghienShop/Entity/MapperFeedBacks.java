package NguoiNghienShop.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperFeedBacks implements RowMapper<FeedBacks> {
	public FeedBacks mapRow(ResultSet rs, int rowNum) throws SQLException { 
		FeedBacks feedbacks = new FeedBacks();
		feedbacks.setId(rs.getInt("id"));
		feedbacks.setName(rs.getString("name"));
		feedbacks.setPhone(rs.getString("phone"));
		feedbacks.setEmail(rs.getString("email"));
		feedbacks.setAddress(rs.getString("address"));
		feedbacks.setMessage(rs.getString("message"));
		feedbacks.setCreated_at(rs.getDate("created_at"));
		feedbacks.setStatus(rs.getBoolean("status"));
		return feedbacks;
	}
	

}
