package NguoiNghienShop.Entity;

public class Categorys {
	private int id;  
	private String name;  
	private String mota;  
	private String img;
	
	public Categorys() {
		super();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMota() {
		return mota;
	}
	public void setMota(String mota) {
		this.mota = mota;
	}
	public String getImg() {	
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
}
