package NguoiNghienShop.Admin.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import NguoiNghienShop.Entity.OrderDetails;
import NguoiNghienShop.Entity.Orders;
import NguoiNghienShop.Service.User.IOrdersService;
import NguoiNghienShop.Service.User.IProductService;


@Controller
public class OrdersAdminController extends BasController {
	@Autowired
	public IOrdersService ordersService;
	
	@Autowired
	public IProductService productService;

	@RequestMapping(value = { "/admin/orders" })
	public ModelAndView Index() {
		_mvAdmin.setViewName("admin/orders/index_orders");
		_mvAdmin.addObject("orders", ordersService.GetAllOrders());
		return _mvAdmin;
	}

	@RequestMapping(value = "/admin/saveOrder", method = RequestMethod.POST)
	public ModelAndView saveOrder(@ModelAttribute Orders order) {
		ordersService.saveOrUpdateOrder(order);
		return new ModelAndView("redirect:/admin/orders");

	}


	// Sua
	@RequestMapping(value = "/admin/editOrder", method = RequestMethod.GET)
	public ModelAndView editSlides(HttpServletRequest request) {
		int orderID = Integer.parseInt(request.getParameter("id"));
		Orders order = ordersService.FindOrderById(orderID);
		_mvAdmin.addObject("order", order);
		_mvAdmin.addObject("title", "Chỉnh sửa đơn hàng");
		_mvAdmin.setViewName("admin/orders/form_orders");
		return _mvAdmin;
	}

	// xoa
	@RequestMapping(value = "/admin/deleteOrder", method = RequestMethod.GET)
	public ModelAndView deleteUser(HttpServletRequest request) {
		int Id = Integer.parseInt(request.getParameter("id"));
		ordersService.deleteOrder(Id);
		return new ModelAndView("redirect:/admin/orders");
	}
	
	//xem chi tiet
	@RequestMapping(value = "/admin/viewDetailOrder", method = RequestMethod.GET)
	public ModelAndView viewDetails(HttpServletRequest request) {
		int orderID = Integer.parseInt(request.getParameter("id"));
		List<OrderDetails> detail = ordersService.GetDetailsOrder(orderID);
		_mvAdmin.addObject("detail", detail);
		_mvAdmin.addObject("lstproduct", productService.GetAllProducts());
		_mvAdmin.addObject("title", "Xem chi tiết đơn hàng");
		_mvAdmin.setViewName("admin/orders/index_detail");
		return _mvAdmin;
	}
}
	