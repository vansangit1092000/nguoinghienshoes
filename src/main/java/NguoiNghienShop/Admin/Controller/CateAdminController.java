package NguoiNghienShop.Admin.Controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import NguoiNghienShop.Entity.Categorys;
import NguoiNghienShop.Service.User.ICategoryService;


@Controller
public class CateAdminController extends BasController {
	
	@Autowired
	private ICategoryService categoryService;
	
	@RequestMapping(value = {"/admin/category"})
	public ModelAndView IndexCate() {	
		_mvAdmin.setViewName("admin/category/index_cate");
		_mvAdmin.addObject("cates",categoryService.GetAllCate());

		return _mvAdmin;
	}
	
	@RequestMapping(value = "/admin/saveCate", method = RequestMethod.POST)
	public ModelAndView saveContact(@ModelAttribute Categorys cate) {
		categoryService.SaveOrUpdateCate(cate);
	    return new ModelAndView("redirect:/admin/category");
	   
	}
	
	//Them
	@RequestMapping(value = "/admin/newCate", method = RequestMethod.GET)
	public ModelAndView newContact(ModelAndView model) {
		Categorys newCate = new Categorys();
		_mvAdmin.addObject("cate", newCate);
		_mvAdmin.addObject("title", "Thêm mới danh mục");
		_mvAdmin.setViewName("admin/category/form_cate");
	    	   return _mvAdmin;
	}
	
	//Sua
	@RequestMapping(value = "/admin/editCate", method = RequestMethod.GET)
	public ModelAndView editCate(HttpServletRequest request) {
	    int cateId = Integer.parseInt(request.getParameter("id"));
	    Categorys cate = categoryService.FindCateById(cateId);
	    _mvAdmin.addObject("cate", cate);
	    _mvAdmin.addObject("title", "Chỉnh sửa danh mục");
	    _mvAdmin.setViewName("admin/category/form_cate");
	    return _mvAdmin;
	}
	
	//xoa
	@RequestMapping(value = "/admin/deleteCate", method = RequestMethod.GET)
	public ModelAndView deleteContact(HttpServletRequest request) {
	    int Id = Integer.parseInt(request.getParameter("id"));
	    categoryService.DeleteCate(Id);
	    return new ModelAndView("redirect:/admin/category");
	}
}
