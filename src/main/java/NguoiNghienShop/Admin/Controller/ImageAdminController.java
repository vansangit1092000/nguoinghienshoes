package NguoiNghienShop.Admin.Controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import NguoiNghienShop.Entity.Images;
import NguoiNghienShop.Service.User.IImageService;
import NguoiNghienShop.Service.User.IProductService;

@Controller
public class ImageAdminController extends BasController  {

	@Autowired
	public IImageService imageService;
	
	@Autowired
	public IProductService productService;
	
	@RequestMapping(value = {"/admin/image"})
	public ModelAndView IndexImage() {	
		_mvAdmin.setViewName("admin/image/index_image");
		_mvAdmin.addObject("image", imageService.GetAllImg());
		_mvAdmin.addObject("product", productService.GetAllProducts());
		return _mvAdmin;
	}
	
	@RequestMapping(value = "/admin/saveImage", method = RequestMethod.POST)
	public ModelAndView saveImaget(@ModelAttribute Images image) {
		imageService.saveOrUpdateImage(image);
		return new ModelAndView("redirect:/admin/image");

	}

	// Them
	@RequestMapping(value = "/admin/newImage", method = RequestMethod.GET)
	public ModelAndView newImage(ModelAndView model) {
		Images newImage= new Images();
		_mvAdmin.addObject("image", newImage);
		_mvAdmin.addObject("title", "Thêm mới ảnh");
		_mvAdmin.setViewName("admin/image/form_image");
		return _mvAdmin;
	}
	

	// Sua
	@RequestMapping(value = "/admin/editImage", method = RequestMethod.GET)
	public ModelAndView editImage(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		Images image = imageService.GetIDImage(id);
		_mvAdmin.addObject("image", image);
		_mvAdmin.addObject("title", "Chỉnh sửa ảnh");
		_mvAdmin.setViewName("admin/image/form_image");
		return _mvAdmin;
	}

	// xoa
	@RequestMapping(value = "/admin/deleteImage", method = RequestMethod.GET)
	public ModelAndView deleteImage(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		imageService.DeleteImage(id);
		return new ModelAndView("redirect:/admin/image");
	}
	
	
}
