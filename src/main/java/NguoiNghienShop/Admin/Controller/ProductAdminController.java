package NguoiNghienShop.Admin.Controller;



import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import NguoiNghienShop.Dto.ProductDto;
import NguoiNghienShop.Service.User.ICategoryService;
import NguoiNghienShop.Service.User.IProductService;
import NguoiNghienShop.Service.User.ISizesService;


@Controller
public class ProductAdminController extends BasController {
	
	@Autowired
	public IProductService productService;
	
	@Autowired
	public ICategoryService categoryService;
	
	@Autowired
	public ISizesService sizesService;
	

	
	@RequestMapping(value = {"/admin/product"})
	public ModelAndView IndexProduct() {	
		_mvAdmin.setViewName("admin/product/index_product");
		_mvAdmin.addObject("products",productService.GetAllProducts());
		_mvAdmin.addObject("categorys",categoryService.GetAllCate());
		_mvAdmin.addObject("sizes",sizesService.GetAllSize());
		return _mvAdmin;
	}
	
	@RequestMapping(value = "/admin/saveProduct", method = RequestMethod.POST)
	public ModelAndView saveProduct(@ModelAttribute ProductDto product) {
		productService.saveOrUpdateProduct(product);
		return new ModelAndView("redirect:/admin/product");

	}

	// Them
	@RequestMapping(value = "/admin/newProduct", method = RequestMethod.GET)
	public ModelAndView newProduct(ModelAndView model) {
		ProductDto newproduct= new ProductDto();
		_mvAdmin.addObject("product", newproduct);
		_mvAdmin.addObject("categorys",categoryService.GetAllCate());
		_mvAdmin.addObject("title", "Thêm mới sản phẩm");
		_mvAdmin.setViewName("admin/product/form_product");
		return _mvAdmin;
	}

	// Sua
	@RequestMapping(value = "/admin/editProduct", method = RequestMethod.GET)
	public ModelAndView editProduct(HttpServletRequest request) {
		long id = Long.parseLong(request.getParameter("id"));
		ProductDto product = productService.GetProductById(id);
		_mvAdmin.addObject("product", product);
		_mvAdmin.addObject("categorys",categoryService.GetAllCate());
		_mvAdmin.addObject("title", "Chỉnh sửa sản phẩm");
		_mvAdmin.setViewName("admin/product/form_product");
		return _mvAdmin;
	}

	// xoa
	@RequestMapping(value = "/admin/deleteProduct", method = RequestMethod.GET)
	public ModelAndView deleteProduct(HttpServletRequest request) {
		long id = Long.parseLong(request.getParameter("id"));
		productService.DeleteProduct(id);
		return new ModelAndView("redirect:/admin/product");
	}
}
