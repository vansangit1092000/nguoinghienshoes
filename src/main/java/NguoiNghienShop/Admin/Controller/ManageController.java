package NguoiNghienShop.Admin.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import NguoiNghienShop.Entity.Admin;
import NguoiNghienShop.Entity.User;
import NguoiNghienShop.Service.User.IAdminService;

@Controller
public class ManageController extends BasController{	
		@Autowired
		private IAdminService adminService;
		
		@RequestMapping(value = {"/admin/admin"})
		public ModelAndView IndexAdmin() {	
			_mvAdmin.setViewName("admin/admin/index_admin");
			_mvAdmin.addObject("admin", adminService.GetAllAdmin());
			_mvAdmin.addObject("role", adminService.GetAllRole());		
			return _mvAdmin;
		}
		
		@RequestMapping(value = "/admin/saveAdmin", method = RequestMethod.POST)
		public ModelAndView saveAdmin(@ModelAttribute Admin ad) {
			adminService.saveOrUpdateAdmin(ad);
			return new ModelAndView("redirect:/admin/admin");

		}

		// Them
		@RequestMapping(value = "/admin/newAdmin", method = RequestMethod.GET)
		public ModelAndView newAdmin(ModelAndView model) {
			Admin newAdmin= new Admin();
			_mvAdmin.addObject("admin", newAdmin);
			_mvAdmin.addObject("role", adminService.GetAllRole());	
			_mvAdmin.addObject("title", "Thêm mới Admin");
			_mvAdmin.setViewName("admin/admin/form_admin");
			return _mvAdmin;
		}
		

		// Sua
		@RequestMapping(value = "/admin/editAdmin", method = RequestMethod.GET)
		public ModelAndView editAdmin(HttpServletRequest request) {
			int id = Integer.parseInt(request.getParameter("id"));
			Admin admin = adminService.GetIDAdmin(id);
			_mvAdmin.addObject("admin", admin);
			_mvAdmin.addObject("role", adminService.GetAllRole());	
			_mvAdmin.addObject("title", "Chỉnh sửa Admin");
			_mvAdmin.setViewName("admin/admin/form_admin");
			return _mvAdmin;
		}

		// xoa
		@RequestMapping(value = "/admin/deleteAdmin", method = RequestMethod.GET)
		public ModelAndView deleteAdmin(HttpServletRequest request) {
			int id = Integer.parseInt(request.getParameter("id"));
			adminService.DeleteAdmin(id);
			return new ModelAndView("redirect:/admin/admin");
		}
		
		
		
		@RequestMapping(value = "/admin-login", method = RequestMethod.POST)
		public ModelAndView CheckLogin(HttpSession session, HttpServletRequest request,@ModelAttribute("admin") Admin admin) {
			
			if(admin.getUsername() == "") {
				_mvAdmin.addObject("Statuslogin", "Vui lòng điền đủ thông tin");
				return _mvAdmin;
			}		
			
			Admin check = adminService.GetAccount(admin);
			if (check != null) {
				 session.setAttribute("LoginAdmin", check);
				 _mvAdmin.setViewName("redirect:/admin");
			} else {
				_mvAdmin.addObject("Statuslogin", "Tài khoản hoặc mật khẩu sai !");
			}
			return _mvAdmin;	

		}
		@RequestMapping(value = "/admin-login", method = RequestMethod.GET)
		public ModelAndView Login() {
			Admin admin = new Admin();
			_mvAdmin.addObject(admin);
			_mvAdmin.setViewName("user/account/login");
			return _mvAdmin;
		}
		
		@RequestMapping(value = "/logout", method = RequestMethod.GET)
		public String Login(HttpSession session, HttpServletRequest request) {
			session.removeAttribute("LoginAdmin");
			return "redirect:"+request.getHeader("Referer");
		}
		

}
