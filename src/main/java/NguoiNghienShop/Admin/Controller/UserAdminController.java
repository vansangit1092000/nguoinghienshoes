package NguoiNghienShop.Admin.Controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import NguoiNghienShop.Entity.User;
import NguoiNghienShop.Service.User.IUserService;

@Controller
public class UserAdminController extends BasController {
	
	@Autowired
	public IUserService userService;
	
	@RequestMapping(value = {"/admin/user"})
	public ModelAndView IndexAdmin() {	
		_mvAdmin.setViewName("admin/user/index_user");
		_mvAdmin.addObject("user",userService.GetAllUser());
		return _mvAdmin;
	}
	
	@RequestMapping(value = "/admin/saveUser", method = RequestMethod.POST)
	public ModelAndView saveContact(@ModelAttribute User user) {
		userService.saveOrUpdateUser(user);
	    return new ModelAndView("redirect:/admin/user");
	   
	}
		
	//Sua
	@RequestMapping(value = "/admin/editUser", method = RequestMethod.GET)
	public ModelAndView editUser(HttpServletRequest request) {
	    int userId = Integer.parseInt(request.getParameter("id"));
	    User user = userService.FindUserById(userId);
	    _mvAdmin.addObject("user", user);
	    _mvAdmin.addObject("title", "Chỉnh sửa người dùng");
	    _mvAdmin.setViewName("admin/user/form_user");
	    return _mvAdmin;
	}
	
	//xoa
	@RequestMapping(value = "/admin/deleteUser", method = RequestMethod.GET)
	public ModelAndView deleteUser(HttpServletRequest request) {
	    int Id = Integer.parseInt(request.getParameter("id"));
	    userService.deleteUser(Id);
	    return new ModelAndView("redirect:/admin/user");
	}
}
