package NguoiNghienShop.Admin.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeAdminController extends BasController {
	
	@RequestMapping(value = {"/admin"})
	public ModelAndView IndexAdmin() {	
		_mvAdmin.setViewName("admin/index_admin");		
		return _mvAdmin;
	}
}
