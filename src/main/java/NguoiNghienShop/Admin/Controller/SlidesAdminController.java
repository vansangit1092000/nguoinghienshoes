package NguoiNghienShop.Admin.Controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import NguoiNghienShop.Entity.Slides;
import NguoiNghienShop.Service.User.ISlidesService;


@Controller
public class SlidesAdminController extends BasController {
	@Autowired
	public ISlidesService slidesService;

	@RequestMapping(value = { "/admin/slides" })
	public ModelAndView Index() {
		_mvAdmin.setViewName("admin/slides/index_slides");
		_mvAdmin.addObject("slides", slidesService.GetDataSlide());
		return _mvAdmin;
	}

	@RequestMapping(value = "/admin/saveSlides", method = RequestMethod.POST)
	public ModelAndView saveSlides(@ModelAttribute Slides slides) {
		slidesService.SaveOrUpdateSlides(slides);
		return new ModelAndView("redirect:/admin/slides");

	}

	// Them
	@RequestMapping(value = "/admin/newSlides", method = RequestMethod.GET)
	public ModelAndView newSlides(ModelAndView model) {
		Slides newSlides = new Slides();
		_mvAdmin.addObject("slides", newSlides);
		_mvAdmin.addObject("title", "Thêm mới Slides");
		_mvAdmin.setViewName("admin/slides/form_slides");
		return _mvAdmin;
	}

	// Sua
	@RequestMapping(value = "/admin/editSlides", method = RequestMethod.GET)
	public ModelAndView editSlides(HttpServletRequest request) {
		int slidesID = Integer.parseInt(request.getParameter("id"));
		Slides slides = slidesService.FindSlidesById(slidesID);
		_mvAdmin.addObject("slides", slides);
		_mvAdmin.addObject("title", "Chỉnh sửa Slides");
		_mvAdmin.setViewName("admin/slides/form_slides");
		return _mvAdmin;
	}

	// xoa
	@RequestMapping(value = "/admin/deleteSlides", method = RequestMethod.GET)
	public ModelAndView deleteUser(HttpServletRequest request) {
		int Id = Integer.parseInt(request.getParameter("id"));
		slidesService.DeleteSlides(Id);
		return new ModelAndView("redirect:/admin/slides");
	}
}
